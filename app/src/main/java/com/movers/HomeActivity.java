package com.movers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.movers.base.BaseActivity;
import com.movers.base.RootView;
import com.movers.common.dialog.CustoomButtonDialog;
import com.movers.common.dialog.SingleButtonDialog;
import com.movers.common.dialog.TwoButtonDialog;
import com.movers.core.Session;
import com.movers.di.component.ActivityComponent;
import com.movers.ui.authentication.adapter.DrawerAdapter;
import com.movers.ui.authentication.fragment.AboutUsFragment;
import com.movers.ui.authentication.fragment.AddLoadJobPostFragment;
import com.movers.ui.authentication.fragment.AddTruckSpaceFragment;
import com.movers.ui.authentication.fragment.ContactUsFragment;
import com.movers.ui.authentication.fragment.DriverModuleFragment;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.LoginFragment;
import com.movers.ui.authentication.fragment.MyBillingFragment;
import com.movers.ui.authentication.fragment.MyLoadJobFragment;
import com.movers.ui.authentication.fragment.MyNetworkFragment;
import com.movers.ui.authentication.fragment.MyProfileFragment;
import com.movers.ui.authentication.fragment.MyRattingFragment;
import com.movers.ui.authentication.fragment.ShareFragment;
import com.movers.ui.authentication.model.DummyParentDataItem;
import com.movers.utilsfile.Constant;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.Disposable;

public class HomeActivity extends BaseActivity implements
        DrawerAdapter.DrawerItemClickListener {
    @BindView(R.id.drawer_layout)
    public DrawerLayout drawerLayout;
    public RootView rootView;
    public DrawerAdapter drawerAdapter;
    @BindView(R.id.recyclerViewHome)
    public RecyclerView recyclerViewHome;

    @BindView(R.id.llProfile)
    public LinearLayout llProfile;

    @BindView(R.id.txtName)
    public AppCompatTextView txtName;

    @BindView(R.id.txtEmail)
    public AppCompatTextView txtEmail;
    @Inject
    Session session;

    @Override
    public int findFragmentPlaceHolder() {
        return R.id.placeHolder;
    }

    @Override
    public int findContentView() {
        return R.layout.activity_home;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
        drawerLayout.setDrawerElevation(0);
        setUpDrawerSlide();
        setUpDrawer();
        txtName.setText(session.getFname() + session.getLastName());
        txtEmail.setText(session.getEmail());
        rootView = new RootView() {
            @Override
            public void showMessage(String message) {

            }

            @Override
            public void showLoader() {
            }

            @Override
            public void hideLoader() {
            }

            @Override
            public void hideKeyBoard() {

            }

            @Override
            public void showKeyBoard() {
            }

            @Override
            public void onError(Throwable throwable) {
            }

            @Override
            public void addDisposable(Disposable disposable) {
            }


            @Override
            public void openSimpleDialog(String message, SingleButtonDialog.SingleButtonListener listener) {

            }

            @Override
            public void openCustomDialog(String message, CustoomButtonDialog.SingleButtonListener listener) {

            }

        };
        load(HomeFragment.class, false).replace(true, Constant.HOME);
        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                load(MyProfileFragment.class, false).replace(true, Constant.OTHER_FRAGMENT);
            }
        });
    }

    @Override
    public void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    private ArrayList<DummyParentDataItem> getDummyDataToPass() {
        ArrayList<DummyParentDataItem> arrDummyData = new ArrayList<>();
        arrDummyData.add(new DummyParentDataItem(getResources().getString(R.string.home), R.drawable.homr));
        arrDummyData.add(new DummyParentDataItem(getResources().getString(R.string.my_load_job_menu), R.drawable.my_load_case));
        arrDummyData.add(new DummyParentDataItem(getResources().getString(R.string.truck_space), R.drawable.truck_menu));
        arrDummyData.add(new DummyParentDataItem(getResources().getString(R.string.my_bill), R.drawable.my_belling));
        arrDummyData.add(new DummyParentDataItem(getResources().getString(R.string.my_rat), R.drawable.my_ratting));
        arrDummyData.add(new DummyParentDataItem(getResources().getString(R.string.my_network), R.drawable.my_network));
        arrDummyData.add(new DummyParentDataItem(getResources().getString(R.string.driver_module), R.drawable.driver_menu));
        arrDummyData.add(new DummyParentDataItem(getResources().getString(R.string.about_us), R.drawable.about_us_menu));
        arrDummyData.add(new DummyParentDataItem(getResources().getString(R.string.contact_us), R.drawable.contact_us_menu));
        arrDummyData.add(new DummyParentDataItem(getResources().getString(R.string.share_app), R.drawable.share));
        arrDummyData.add(new DummyParentDataItem(getResources().getString(R.string.logout), R.drawable.logout));
        return arrDummyData;
    }

    private void logout() {
        getCurrentFragment().openAlertDialog(getResources().getString(R.string.are_you_sure_want_to_logout),
                new TwoButtonDialog.TwoButtonListener() {
                    @Override
                    public void onYes() {
                        LoginManager.getInstance().logOut();
                        session.setLogin(false);
                        session.clearSession();

                      //  HomeActivity.this.finish();
                    }

                    @Override
                    public void onNo() {

                    }
                });
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBackPressed() {
        Log.e("TAG", "onBackPressed: " + getCurrentFragment().getTag());
        if (getCurrentFragment().getTag() != null && getCurrentFragment().getTag().equals(Constant.HOME)) {
            if (drawerLayout.isDrawerOpen(Gravity.START)) {
                drawerLayout.closeDrawer(Gravity.START);
            }
            getCurrentFragment().openAlertDialog(getResources().getString(R.string.are_you_sure_want_to_logout),
                    new TwoButtonDialog.TwoButtonListener() {
                        @Override
                        public void onYes() {
                            HomeActivity.this.finish();
                        }

                        @Override
                        public void onNo() {

                        }
                    });
        } else {
            try {
                super.onBackPressed();
            } catch (Exception e) {
            }
        }
    }

    @SuppressLint("WrongConstant")
    @Override
    public void toggleDrawer() {
        if (drawerLayout.isDrawerOpen(Gravity.START))
            drawerLayout.closeDrawer(Gravity.START);
        else
            drawerLayout.openDrawer(Gravity.START);
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onDrawerItem(int item) {
        switch (item) {
            case 0:
                load(HomeFragment.class, false).replace(true, Constant.OTHER_FRAGMENT);
                break;
            case 1:
                load(MyLoadJobFragment.class, false).replace(true, Constant.OTHER_FRAGMENT);
                break;
            case 2:
                load(AddTruckSpaceFragment.class, false).replace(true, Constant.OTHER_FRAGMENT);
                break;
            case 3:
                load(MyBillingFragment.class, false).replace(true, Constant.OTHER_FRAGMENT);
                break;
            case 4:
                load(MyRattingFragment.class, false).replace(true, Constant.OTHER_FRAGMENT);
                break;
            case 5:
                load(MyNetworkFragment.class, false).replace(true, Constant.OTHER_FRAGMENT);
                break;
            case 6:
                load(DriverModuleFragment.class, false).replace(true, Constant.OTHER_FRAGMENT);
                break;
            case 7:
                load(AboutUsFragment.class, false).replace(true, Constant.OTHER_FRAGMENT);
                break;
            case 8:
                load(ContactUsFragment.class, false).replace(true, Constant.OTHER_FRAGMENT);
                break;

            case 9:
                load(ShareFragment.class, false).replace(true, Constant.OTHER_FRAGMENT);
                break;

            case 10:
                logout();
                break;
        }
        drawerLayout.closeDrawer(Gravity.START);
    }

    private void setUpDrawer() {
        drawerAdapter = new DrawerAdapter(HomeActivity.this, getDummyDataToPass(), this);
        recyclerViewHome.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewHome.setAdapter(drawerAdapter);
    }

    private void setUpDrawerSlide() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.Open, R.string.Close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }
        };
        drawerLayout.addDrawerListener(toggle);

    }
}