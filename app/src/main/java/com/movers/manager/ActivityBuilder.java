package com.movers.manager;

import android.os.Bundle;
import androidx.annotation.StyleRes;
import androidx.annotation.UiThread;
import androidx.core.util.Pair;
import android.view.View;


import com.movers.base.BaseFragment;

import java.util.List;

/**
 * Created by hlink21 on 11/5/16.
 */
@UiThread
public interface ActivityBuilder {

    void start();

    ActivityBuilder addBundle(Bundle bundle);

    ActivityBuilder addSharedElements(List<Pair<View, String>> pairs);

    ActivityBuilder byFinishingCurrent();

    ActivityBuilder byFinishingAll();

    <T extends BaseFragment> ActivityBuilder setPage(Class<T> page);

    ActivityBuilder forResult(int requestCode);

    ActivityBuilder shouldAnimate(boolean isAnimate);

    ActivityBuilder setTheme(@StyleRes int color);

}
