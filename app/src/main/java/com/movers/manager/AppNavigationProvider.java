package com.movers.manager;

import androidx.annotation.StyleRes;

/**
 * Created by hlink21 on 29/5/17.
 */

public interface AppNavigationProvider {


    interface HasPage {
    }

    interface HasThemeColor {
        String THEME_COLOR = "THEME_COLOR";

        @StyleRes
        int getTheme();
    }
}
