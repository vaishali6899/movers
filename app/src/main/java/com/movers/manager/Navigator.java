package com.movers.manager;



import com.movers.base.BaseActivity;
import com.movers.base.BaseFragment;


/**
 * Created by hlink21 on 29/5/17.
 */

public interface Navigator {


    <T extends BaseFragment> FragmentActionPerformer<T> load(Class<T> tClass, boolean b);

    ActivityBuilder loadActivity(Class<? extends BaseActivity> aClass);

    <T extends BaseFragment> ActivityBuilder loadActivity(Class<? extends BaseActivity> aClass, Class<T> pageTClass);

    void showErrorMessage(String message);

    void toggleLoader(boolean show);

    void goBack();

    void finish();

    void toggleDrawer();

}
