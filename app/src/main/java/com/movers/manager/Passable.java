package com.movers.manager;

/**
 * Created by hlink21 on 27/5/16.
 */
public interface Passable<T> {

    void passData(T t);

}
