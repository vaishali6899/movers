package com.movers.utilsfile;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.movers.R;

public class CustomProgressDialogue extends Dialog {


    ProgressBar progress_bar;
    String TAG="CustomProgressDialogue";

    public CustomProgressDialogue(Context context) {
        super(context);

        WindowManager.LayoutParams wlmp = getWindow().getAttributes();

        //wlmp.gravity = Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(wlmp);
        setTitle(null);
        setCancelable(false);
        setOnCancelListener(null);
        View view = LayoutInflater.from(context).inflate(
                R.layout.custom_progress, null);
        progress_bar=(ProgressBar)view.findViewById(R.id.progress_bar);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            progress_bar.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.text_color), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        setContentView(view);


    }
}
