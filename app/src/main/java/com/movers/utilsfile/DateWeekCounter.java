package com.movers.utilsfile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateWeekCounter {

	static String TAG="DateWeekCounter";
	public DateWeekCounter() {
	}


	public static String getDatefromMilsecDidsplay(long date_milisec) {
		Calendar calendar=Calendar.getInstance();
		calendar.setTimeInMillis(date_milisec);
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM");
		SimpleDateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy");
		df.setTimeZone(TimeZone.getDefault());
		df1.setTimeZone(TimeZone.getDefault());

        String returndate="";
        int year=Calendar.getInstance().get(Calendar.YEAR);
        if(Integer.parseInt(df2.format(calendar.getTime()))==year){
            returndate=df.format(calendar.getTime());
        }else{
            returndate=df1.format(calendar.getTime());
        }

		return returndate;
	}



	public static String getCurrentMonthenddate() {
		Calendar c1 = Calendar.getInstance();
		//first day of week
		c1.set(Calendar.DAY_OF_MONTH,
				c1.getActualMinimum(Calendar.DAY_OF_MONTH));
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM");
		df.setTimeZone(TimeZone.getDefault());
		String startdate = df.format(c1.getTime());

		c1.set(Calendar.DAY_OF_MONTH,
				c1.getActualMaximum(Calendar.DAY_OF_MONTH));
		df.setTimeZone(TimeZone.getDefault());
		String endate = df.format(c1.getTime());
		return startdate+" - "+endate;
	}

	public static String getCurrentDateMilisec(int totalSecs){
		int hours = totalSecs / 3600;
		int minutes = (totalSecs % 3600) / 60;
		int seconds = totalSecs % 60;
		String timeString="";

		timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);

		return timeString;
	}

	public static String getDaynamefromDate(String date1) {
		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date date = null;
		try {
			date = inFormat.parse(date1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
		String goal = outFormat.format(date);
		return goal;
	}

	public static String getDate(String date1) {
		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		try {
			date = inFormat.parse(date1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat outFormat = new SimpleDateFormat("dd/MM/YYYY");
		String goal = outFormat.format(date);
		return goal;
	}

	public static String getTime(String date1) {
		SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		try {
			date = inFormat.parse(date1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SimpleDateFormat outFormat = new SimpleDateFormat("hh : mm a");
		String goal = outFormat.format(date);
		return goal;
	}



	public static String getTimeFromDate(String datetime) {
		String datesting = "";
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		try {
			Date date = null;
			date = df.parse(datetime);
			SimpleDateFormat sdf = new SimpleDateFormat("hh : mm a");
			String shortTimeStr = sdf.format(date);
			datesting = shortTimeStr;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return datesting;
	}


	public static String getDatefromDate(String startTime) {

		String datesting = "";
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			Date date = null;
			date = df.parse(startTime);
			SimpleDateFormat sdf = new SimpleDateFormat("E dd-MMM-yyy HH:mm");
			sdf.setTimeZone(TimeZone.getDefault());
			String shortTimeStr = sdf.format(date);
			datesting = shortTimeStr;
		} catch (ParseException e) {
			// To change body of catch statement use File | Settings | File Templates.
			e.printStackTrace();
		}
		return datesting;
	}

}
