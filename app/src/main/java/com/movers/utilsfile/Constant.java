package com.movers.utilsfile;

import android.content.Intent;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;

public class Constant {

    public static final int INTENT_FLAGS = Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP
            | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK;
    public static final String TWITTER = "3";
    public static final String FACEBOOOK = "1";
    public static final String GOOGLE = "2";
    public static final String NORMAL = "0";
    public static final String PAGE = "page";
    public static final String PARTY_DETAILS = "partydetails";
    public static final String OTHER_PROFILEDETAILS = "otherprofile_detals";
    public static final String BUSSIEST_LOCATIONSCREEN = "bussiest_location_screen";
    public static final String OPEN_IMAGE_SCREEN = "open_image_screen";
    public static final String LOGIN_SCREEN = "login_screen";
    public static final String UPLOAD_IMAGE_SCREEN = "upload_image_screen";
    public static final String FORGOTPASSWORD_SCREEN = "forgot_password_screen";
    public static final String OTP_SCREEN = "otp_screen";
    public static final String FIREBASE_PUSH_NOTIFICATION = "notification_broadcast";
    public static final int GPS_REQUEST =1472 ;
    public static String REGISTER_TYPE_NORMAL ="" ;
    public static final int PERMISSION_ID = 44;
    public static String CLICK_LOCATION_TYPE = "0";
    public static  String LOCATION_CLICK_SEE_DATA ="0" ;
    public static String DELETE_ICON_SEE_STATUS = "0";
    public static String HOME = "home";
    public static final String ABOUT_US = "about_us";
    public static final String SEARCH = "search";
    public static final String PROFILE = "profile";
    public static final String FAVOURITES = "favourites";
    public static final String INVITE_FRIENDS = "invite_friends";
    public static final String RATE_US = "rate_us";
    public static final String TERM_AND_CONDITIONS = "term_and_conditions";
    public static final String PRIVACY_POLICY = "privacy_policy";
    public static final String ABOUTUS_ID = "1";
    public static final String PRIVACY_POLICIS_ID = "3";
    public static final String TERMCONDITION_ID = "2";
    public static final int PERMISSION_FOR_LOCATION = 2;
    public static String home = "home";
    public static String add = "add";
    public static String data = "data";
    public static String android = "A";
    public static String OTHER_FRAGMENT = "other_fragment";
    public static GoogleSignInClient mGoogleSignInClient;
    public static String UserType="carrier";
}
