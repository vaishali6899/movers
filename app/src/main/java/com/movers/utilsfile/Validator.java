package com.movers.utilsfile;

import android.util.Patterns;
import android.widget.TextView;

import com.movers.exception.ApplicationException;
/**
 * Created by Vishwa on 11/7/18.
 */
public class Validator {
    private final TextView textView;

    public Validator(TextView textView) {
        this.textView = textView;
    }

    public static Validator init(TextView textView) {
        return new Validator(textView);
    }

    public void checkEmpty(String message) throws ApplicationException {
        if (textView.getText().toString().trim().isEmpty()) {
            textView.requestFocus();
            throw new ApplicationException(message);
        }
    }

    public void checkPhone(String message) throws ApplicationException {
        if(message!=null && message.length()==10) {

        }else {
            if (textView.getText().toString().trim().isEmpty()) {
                textView.requestFocus();
                throw new ApplicationException(message);
            }
        }

    }

    public void checkMinLength(int min, String message) throws ApplicationException {
        if (textView.getText().toString().length() < min) {
            textView.requestFocus();
            throw new ApplicationException(message);
        }
    }

    public void checkRegex(String regex, String message) throws ApplicationException {
        if (textView.getText().toString().matches(regex)) {
            textView.requestFocus();
            throw new ApplicationException(message);
        }
    }

    public void checkMaxLength(int max, String message) throws ApplicationException {
        if (textView.getText().toString().length() != max) {
            textView.requestFocus();
            throw new ApplicationException(message);
        }
    }

    public void checkEmptyWithoutTrim(String message) throws ApplicationException {
        if (textView.getText().toString().isEmpty()) {
            textView.requestFocus();
            throw new ApplicationException(message);
        }
    }

    public void checkMail(String message) throws ApplicationException {
        if (!textView.getText().toString().trim().matches(Patterns.EMAIL_ADDRESS.pattern())) {
            textView.requestFocus();
            throw new ApplicationException(message);
        }
    }

    public void compare(TextView textView1, String message) throws ApplicationException {
        if (!textView1.getText().toString().equals(textView.getText().toString())){
            textView1.setText("");
            textView1.requestFocus();
            throw new ApplicationException(message);
        }
    }
    public void shouldnosame(TextView textView1, String message) throws ApplicationException {
        if (textView1.getText().toString().equals(textView.getText().toString())){
            textView1.setText("");
            textView1.requestFocus();
            throw new ApplicationException(message);
        }
    }

    public void checkValidWeb(String data,String message) throws ApplicationException {
        if(data!=null && data.length()>0){
            if(!Patterns.WEB_URL.matcher(data).matches()) {
                throw new ApplicationException(message);
            }
        }else{
            throw new ApplicationException(message);
        }
    }
}
