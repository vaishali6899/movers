package com.movers.utilsfile;

import android.util.Log;

public class AppLog {

    public static final boolean IS_DEGUG = true;

    public static final void Log(String tag, String messge) {
            if (IS_DEGUG) {
                try {
                    Log.i(tag, messge);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
    }
}
