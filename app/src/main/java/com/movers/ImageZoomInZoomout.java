package com.movers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageZoomInZoomout extends AppCompatActivity {

    Context appContext;
    Intent intent;
    String TAG = "ImageZoomInZoomout";

    String image = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_zoom_in_zoomout);
        ButterKnife.bind(this);

    }


    @OnClick({})
    public void onViewClicked(View view) {
        switch (view.getId()) {
        }
    }
}