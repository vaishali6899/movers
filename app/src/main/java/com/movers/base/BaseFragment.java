package com.movers.base;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Toast;

import com.movers.common.dialog.CustoomButtonDialog;
import com.movers.utilsfile.AppLog;
import com.movers.common.dialog.SingleButtonDialog;
import com.movers.common.dialog.TwoButtonDialog;
import com.movers.core.AppPreferences;
import com.movers.core.Session;
import com.movers.di.HasComponent;
import com.movers.di.component.ActivityComponent;
import com.movers.di.component.FragmentComponent;
import com.movers.di.module.FragmentModule;
import com.movers.exception.ApplicationException;
import com.movers.exception.AuthenticationException;
import com.movers.exception.ServerException;
import com.movers.exception.SuccessException;
import com.movers.exception.UserNotException;
import com.movers.manager.FragmentHandler;
import com.movers.manager.FragmentManager;
import com.movers.manager.FragmentNavigationFactory;


import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by hlink21 on 25/4/16.
 */
public abstract class BaseFragment<PresenterT extends BasePresenter<ViewT>, ViewT extends RootView> extends Fragment
        implements RootView, HasComponent<FragmentComponent> {


    @Inject
    protected PresenterT presenter;
    @Inject
    protected AppPreferences appPreferences;
    protected HasToolbar toolbar;
    protected Unbinder bind;
    String TAG = "BaseFragment";
    @Inject
    Session session;
    ActivityManager mActivityManager;
    @Inject
    @Named("child_fragment_handler")
    FragmentHandler childFragmentHandler;
    @Inject
    FragmentNavigationFactory fragmentNavigationFactory;
    private CompositeDisposable compositeDisposable;

    List<ApplicationInfo> packages;
    PackageManager pm;

    private FragmentComponent fragmentComponent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(createLayout(), container, false);
        bind = ButterKnife.bind(this, view);
        pm = getActivity().getPackageManager();
        packages = pm.getInstalledApplications(0);
        mActivityManager = (ActivityManager) getContext().getSystemService(Context.ACTIVITY_SERVICE);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (presenter != null) {
            presenter.setView(createView());
        }
        bindData();
    }

    @Override
    public FragmentComponent getComponent() {
        if (fragmentComponent == null) {
            if (getActivity() instanceof HasComponent) {
                ActivityComponent component = getComponent(ActivityComponent.class);
                fragmentComponent = component.plus(new FragmentModule(this));
            }
        }
        return fragmentComponent;
    }

    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    public FragmentHandler getChildFragmentHandler() {
        return childFragmentHandler;
    }

    public int getChildPlaceHolder() {
        return 0;
    }

    @Override
    public void onAttach(Context context) {
        inject(getComponent());

        // attach child fragment manager
        if (fragmentNavigationFactory == null)
            throw new RuntimeException("make sure you have inject the fragment in inject() method");
        else
            fragmentNavigationFactory.attachChildFragmentHandler(childFragmentHandler);

        super.onAttach(context);
        compositeDisposable = new CompositeDisposable();

        if (getActivity() instanceof HasToolbar)
            toolbar = (HasToolbar) getActivity();


    }


    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (FragmentManager.sDisableFragmentAnimations) {
            Animation a = new Animation() {
            };
            a.setDuration(0);
            return a;
        }
        return super.onCreateAnimation(transit, enter, nextAnim);
    }


    public void openAlertDialog(String message, TwoButtonDialog.TwoButtonListener listener) {
        hideKeyBoard();
        TwoButtonDialog dialog = new TwoButtonDialog(message);
        dialog.listener = listener;
        dialog.message = message;
        Log.e(TAG, "message: "+ message );
        dialog.show(getChildFragmentManager().beginTransaction(), dialog.getClass().getSimpleName());
    }

    @Override
    public void onDestroy() {

        if (presenter != null) {
            presenter.destroy();
            presenter = null;
            compositeDisposable.dispose();
        }
        if (bind != null)
            bind.unbind();

        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter != null)
            presenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (presenter != null)
            presenter.pause();
    }

    @Override
    public void showMessage(String message) {
        if (presenter != null)
            presenter.navigator.showErrorMessage(message);
    }


    @Override
    public void showLoader() {

        if (presenter != null)
            presenter.navigator.toggleLoader(true);
    }

    @Override
    public void hideLoader() {

        if (presenter != null)
            presenter.navigator.toggleLoader(false);

    }


    @Override
    public void hideKeyBoard() {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).hideKeyboard();
        }
    }

    @Override
    public void showKeyBoard() {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showKeyboard();
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onError(Throwable throwable) {

        try {
            if (throwable instanceof ServerException) {
                AppLog.Log(TAG, "Inside ServerException");
                showMessage(throwable.getMessage());
            } else if (throwable instanceof SuccessException) {
                AppLog.Log(TAG, "Inside ServerException");
                // showMessage(throwable.getMessage());
            } else if (throwable instanceof ConnectException) {
                showMessage("connect to internet");
            } else if (throwable instanceof ApplicationException) {
                showMessage(throwable.getMessage());
            } else if (throwable instanceof AuthenticationException) {
                appPreferences.clearAll();
                session.clearSession();
                for (ApplicationInfo packageInfo : packages) {
                    mActivityManager.killBackgroundProcesses(packageInfo.packageName);
                }
                Toast.makeText(getActivity(), "Seems like your request is not valid. Please login here and try again", Toast.LENGTH_LONG).show();
            } else if (throwable instanceof UserNotException) {
                appPreferences.clearAll();
                session.clearSession();
                for (ApplicationInfo packageInfo : packages) {
                    mActivityManager.killBackgroundProcesses(packageInfo.packageName);
                }
                Toast.makeText(getActivity(), "User not found. Please login here.", Toast.LENGTH_SHORT).show();
                AppLog.Log(TAG, "Inside Usernot found exception");
            } else if (throwable instanceof SocketTimeoutException) {
                showMessage("Connection time out");
            } else showMessage("Something wrong here");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public <T extends BaseFragment> T getParentFragment(Class<T> targetFragment) {
        if (getParentFragment() == null) return null;
        return targetFragment.cast(getParentFragment());
    }

    public void onShow() {

    }


    public void openSimpleDialog(String message, SingleButtonDialog.SingleButtonListener listener) {
        hideKeyBoard();
        SingleButtonDialog singleButtonDialog = new SingleButtonDialog();
        singleButtonDialog.listener = listener;
        singleButtonDialog.message = message;
        singleButtonDialog.show(getChildFragmentManager().beginTransaction(), singleButtonDialog.getClass().getSimpleName());
    }

    public void openCustomDialog(String message, CustoomButtonDialog.SingleButtonListener listener) {
        hideKeyBoard();
        CustoomButtonDialog singleButtonDialog = new CustoomButtonDialog();
        singleButtonDialog.listener = listener;
        singleButtonDialog.message = message;
        singleButtonDialog.show(getChildFragmentManager().beginTransaction(), singleButtonDialog.getClass().getSimpleName());
    }

    @Override
    public void addDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    protected boolean onBackActionPerform() {
        return true;
    }

    protected abstract int createLayout();

    protected abstract void inject(FragmentComponent fragmentComponent);

    protected abstract ViewT createView();

    protected abstract void bindData();

}
