package com.app.GoGreen.ui.authentication.presenter;

import com.app.GoGreen.base.BasePresenter_MembersInjector;
import com.app.GoGreen.core.Session;
import com.app.GoGreen.data.repository.UserRepository;
import com.app.GoGreen.manager.Navigator;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final jclass ResetPasswordPresenter_MembersInjector
    implements MembersInjector<ResetPasswordPresenter> {
  private final Provider<Navigator> navigatorProvider;

  private final Provider<Session> sessionProvider;

  private final Provider<UserRepository> repositoryProvider;

  public ResetPasswordPresenter_MembersInjector(
      Provider<Navigator> navigatorProvider,
      Provider<Session> sessionProvider,
      Provider<UserRepository> repositoryProvider) {
    this.navigatorProvider = navigatorProvider;
    this.sessionProvider = sessionProvider;
    this.repositoryProvider = repositoryProvider;
  }

  public static MembersInjector<ResetPasswordPresenter> create(
      Provider<Navigator> navigatorProvider,
      Provider<Session> sessionProvider,
      Provider<UserRepository> repositoryProvider) {
    return new ResetPasswordPresenter_MembersInjector(
        navigatorProvider, sessionProvider, repositoryProvider);
  }

  @Override
  public void injectMembers(ResetPasswordPresenter instance) {
    BasePresenter_MembersInjector.injectNavigator(instance, navigatorProvider.get());
    injectSession(instance, sessionProvider.get());
    injectRepository(instance, repositoryProvider.get());
  }

  public static void injectSession(ResetPasswordPresenter instance, Session session) {
    instance.session = session;
  }

  public static void injectRepository(ResetPasswordPresenter instance, UserRepository repository) {
    instance.repository = repository;
  }
}
