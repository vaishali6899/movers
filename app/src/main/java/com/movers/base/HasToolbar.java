package com.movers.base;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;

/**
 * Created by hlink21 on 20/12/16.
 */

public interface HasToolbar {
    void setToolbar(Toolbar toolbar);

    void showToolbar(boolean b);

    void setToolbarTitle(@NonNull CharSequence title);

    void setToolbarTitle(@NonNull String title);

    void setToolbarTitle(@StringRes int title);

    void showBackButton(boolean b);

    void setToolbarColor(@ColorRes int color);

    void setDrawerToolbarColor(@ColorRes int color);

    void setToolbarElevation(boolean isVisible);
}
