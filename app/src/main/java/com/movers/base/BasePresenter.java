package com.movers.base;



import com.movers.manager.Navigator;

import javax.inject.Inject;


/**
 * Created by hlink21 on 25/4/16.
 */
public abstract class BasePresenter<ViewT extends RootView> {

    protected ViewT view;

    @Inject
    protected Navigator navigator;

    public void setNavigator(Navigator navigator) {
        this.navigator = navigator;
    }

    public void setView(ViewT view) {
        this.view = view;
    }

    public void resume() {

    }

    public void toggleDrawer()
    {

        navigator.toggleDrawer();
    }


    public void pause() {

    }

    public void destroy() {

    }

    public void goBack() {
        navigator.goBack();
    }

}
