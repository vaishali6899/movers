package com.movers.di.component;


import com.movers.di.PerFragment;
import com.movers.di.module.FragmentModule;
import com.movers.base.BaseFragment;
import com.movers.ui.authentication.fragment.AboutUsFragment;
import com.movers.ui.authentication.fragment.AddLoadJobPostFragment;
import com.movers.ui.authentication.fragment.AddTruckSpaceFragment;
import com.movers.ui.authentication.fragment.AutoShippersFragment;
import com.movers.ui.authentication.fragment.BrokerSingInFragment;
import com.movers.ui.authentication.fragment.BrokerSingUpFragment;
import com.movers.ui.authentication.fragment.BrokerSplashFragment;
import com.movers.ui.authentication.fragment.BrokerSplashSecondFragment;
import com.movers.ui.authentication.fragment.BrokerSplashoneFragment;
import com.movers.ui.authentication.fragment.CompanyDetailFragment;
import com.movers.ui.authentication.fragment.CompanyInformationFragment;
import com.movers.ui.authentication.fragment.ContactUsFragment;
import com.movers.ui.authentication.fragment.DriverModuleFragment;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.LoginFragment;
import com.movers.ui.authentication.fragment.MyBillingFragment;
import com.movers.ui.authentication.fragment.MyLoadJobFragment;
import com.movers.ui.authentication.fragment.MyNetworkFragment;
import com.movers.ui.authentication.fragment.MyProfileFragment;
import com.movers.ui.authentication.fragment.MyRattingFragment;
import com.movers.ui.authentication.fragment.NotificationFragment;
import com.movers.ui.authentication.fragment.OTPVerifyFragment;
import com.movers.ui.authentication.fragment.SelectUserFragment;
import com.movers.ui.authentication.fragment.ShareFragment;
import com.movers.ui.authentication.fragment.SingUpFragment;
import com.movers.ui.authentication.fragment.SplashFragment;

import dagger.Subcomponent;

/**
 * Created by hlink21 on 31/5/16.
 */

@PerFragment
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {

    BaseFragment baseFragment();

    void inject(SplashFragment splashFragment);

    void inject(LoginFragment loginFragment);

    void inject(SingUpFragment singUpFragment);

    void inject(CompanyInformationFragment companyInformationFragment);

    void inject(HomeFragment homeFragment);

    void inject(CompanyDetailFragment companyDetailFragment);

    void inject(AutoShippersFragment autoShippersFragment);

    void inject(ContactUsFragment contactUsFragment);

    void inject(ShareFragment shareFragment);

    void inject(DriverModuleFragment driverModuleFragment);

    void inject(MyNetworkFragment myNetworkFragment);

    void inject(MyRattingFragment myRattingFragment);

    void inject(MyBillingFragment myBillingFragment);

    void inject(AboutUsFragment aboutUsFragment);

    void inject(SelectUserFragment aboutUsFragment);

    void inject(AddTruckSpaceFragment aboutUsFragment);

    void inject(AddLoadJobPostFragment aboutUsFragment);

    void inject(MyProfileFragment myProfileFragment);

    void inject(BrokerSingUpFragment brokerSingUpFragment);

    void inject(BrokerSingInFragment brokerSingInFragment);

    void inject(NotificationFragment notificationFragment);

    void inject(OTPVerifyFragment otpVerifyFragment);

    void inject(BrokerSplashFragment brokerSplashFragment);

    void inject(MyLoadJobFragment brokerSplashFragment);

    void inject(BrokerSplashoneFragment brokerSplashFragment);

    void inject(BrokerSplashSecondFragment brokerSplashFragment);

}
