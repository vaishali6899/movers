import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.movers.core.Session;
import com.movers.data.repository.UserRepository;

import java.io.File;
import java.util.Locale;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class,
        NetModule.class,
        ServiceModule.class
})
public interface ApplicationComponent {

  Context context();

  AppPreferences appPreferences();

  @Named("cache")
  File provideCacheDir();

  Resources provideResources();

  Locale provideCurrentLocale();

  Session session();

  UserRepository provideUserRepository();


  @Component.Builder
  interface ApplicationComponentBuilder {

    @BindsInstance
    ApplicationComponentBuilder apiKey(@Named("api-key") String apiKey);

    @BindsInstance
    ApplicationComponentBuilder application(Application application);

    ApplicationComponent build();
  }

}
