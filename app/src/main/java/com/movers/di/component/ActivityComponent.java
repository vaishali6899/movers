package com.movers.di.component;


import com.movers.BrokerSplashActivity;
import com.movers.HomeActivity;
import com.movers.SplashActivity;
import com.movers.di.PerActivity;
import com.movers.di.module.ActivityModule;
import com.movers.di.module.FragmentModule;
import com.movers.base.BaseActivity;
import com.movers.manager.Navigator;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Created by hlink21 on 9/5/16.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = ActivityModule.class)
public interface ActivityComponent {

    BaseActivity activity();

    Navigator navigator();

    FragmentComponent plus(FragmentModule fragmentModule);


    void inject(SplashActivity splashActivity);

    void inject(HomeActivity homeActivity);

    void inject(BrokerSplashActivity brokerSplashActivity);


    @Component.Builder
    interface Builder {

        Builder bindApplicationComponent(ApplicationComponent applicationComponent);

        @BindsInstance
        Builder bindActivity(BaseActivity baseActivity);

        ActivityComponent build();
    }
}
