import android.app.Application;

public enum Injector {
  INSTANCE;
  ApplicationComponent applicationComponent;

  Injector() {
  }

  public void initAppComponent(Application application, String apiKey) {
    applicationComponent = DaggerApplicationComponent.builder()
            .application(application)
            .apiKey(apiKey)
            .build();
  }

  public ApplicationComponent getApplicationComponent() {
    return applicationComponent;
  }
}
