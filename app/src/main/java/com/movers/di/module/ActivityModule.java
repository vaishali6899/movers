package com.movers.di.module;

import androidx.fragment.app.FragmentManager;

import com.movers.di.PerActivity;
import com.movers.base.BaseActivity;
import com.movers.manager.FragmentHandler;
import com.movers.manager.Navigator;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hlink21 on 9/5/16.
 */
@Module
public class ActivityModule {

    @Provides
    @PerActivity
    Navigator navigator(BaseActivity activity) {
        return (Navigator) activity;
    }

    @Provides
    @PerActivity
    FragmentManager fragmentManager(BaseActivity baseActivity) {
        return baseActivity.getSupportFragmentManager();
    }

    @Provides
    @PerActivity
    @Named("placeholder")
    int placeHolder(BaseActivity baseActivity) {
        return baseActivity.findFragmentPlaceHolder();
    }

    @Provides
    @PerActivity
    FragmentHandler fragmentHandler(com.movers.manager.FragmentManager fragmentManager) {
        return fragmentManager;
    }


}
