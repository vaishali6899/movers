package com.movers.di.module;


import com.movers.data.datasource.PartyLocatorDataSource;
import com.movers.data.datasource.UserDataSource;
import com.movers.data.repository.PartyLocatorRepository;
import com.movers.data.repository.UserRepository;
import com.movers.data.service.PartyLocatorService;
import com.movers.data.service.UserService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by hlink21 on 12/5/17.
 */
@Module
public class ServiceModule {


    @Provides
    @Singleton
    UserService provideUserService(Retrofit retrofit) {
        return retrofit.create(UserService.class);
    }

    @Provides
    @Singleton
    UserRepository provideUserRepository(UserDataSource demoDataSource) {
        return demoDataSource;
    }

    @Provides
    @Singleton
    PartyLocatorService provideCollegeService(Retrofit retrofit) {
        return retrofit.create(PartyLocatorService.class);
    }

    @Provides
    @Singleton
    PartyLocatorRepository provideCollegeRepository(PartyLocatorDataSource collegeDataSource) {
        return collegeDataSource;
    }
}
