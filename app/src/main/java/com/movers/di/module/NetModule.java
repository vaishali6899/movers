package com.movers.di.module;


import com.movers.core.AESCryptoInterceptor;
import com.movers.core.Session;
import com.movers.data.URLFactory;
import com.movers.exception.AuthenticationException;
import com.movers.exception.UserNotException;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hlink21 on 11/5/17.
 */


@Module(includes = ApplicationModule.class)
public class NetModule {

    String TAG="NetModule";
    public NetModule() {

    }

    @Singleton
    @Provides
    OkHttpClient provideClient(@Named("header") Interceptor headerInterceptor
            , @Named("pre_validation") Interceptor networkInterceptor
            , @Named("aes") Interceptor aesInterceptor) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient
                .Builder()
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(headerInterceptor)
               // .addInterceptor(aesInterceptor)
                .writeTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(2, TimeUnit.MINUTES)
                .addNetworkInterceptor(networkInterceptor)
                .build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {

        return new Retrofit.Builder()

                .baseUrl(URLFactory.API_PATH)



                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public class NullOnEmptyConverterFactory extends Converter.Factory {

        @Override
        public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
            final Converter<ResponseBody, ?> delegate = retrofit.nextResponseBodyConverter(this, type, annotations);
            return new Converter<ResponseBody, Object>() {
                @Override
                public Object convert(ResponseBody body) throws IOException {
                    if (body.contentLength() == 0) return null;
                    return delegate.convert(body);
                }
            };
        }
    }

    @Provides
    @Singleton
    @Named("header")
    Interceptor provideHeaderInterceptor(final Session session) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                String user = "admin";
                String password = "admin";

                String credentials = Credentials.basic(user, password);

                Request request = chain.request();
                Request build = request.newBuilder()
                        .header("Authorization", credentials).build();
                return chain.proceed(build);
            }
        };
    }

    @Provides
    @Singleton
    @Named("pre_validation")
    Interceptor provideNetworkInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response proceed = chain.proceed(chain.request());
                int code = proceed.code();
                if (code == 401 || code == 403) {
                    throw new AuthenticationException();
                }else if(code==404){
                    throw new UserNotException();
                }
                return proceed;

            }
        };
    }

    @Provides
    @Singleton
    @Named("aes")
    Interceptor provideAesCryptoInterceptor(AESCryptoInterceptor aesCryptoInterceptor) {
        return aesCryptoInterceptor;
    }


}
