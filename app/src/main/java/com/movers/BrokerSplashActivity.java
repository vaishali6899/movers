package com.movers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.movers.base.BaseActivity;
import com.movers.di.component.ActivityComponent;
import com.movers.manager.FragmentManager;
import com.movers.ui.authentication.adapter.ViewPagerAdapter;
import com.movers.ui.authentication.fragment.BrokerSplashSecondFragment;
import com.movers.ui.authentication.fragment.BrokerSplashoneFragment;

public class BrokerSplashActivity extends BaseActivity {
    private ViewPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;


    @Override
    public int findFragmentPlaceHolder() {
        return R.id.placeHolder;
    }

    @Override
    public int findContentView() {
        return R.layout.activity_broker_splash;
    }

    @Override
    public void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);

    }

    @Override
    public void toggleDrawer() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broker_splash);


        viewPager = findViewById(R.id.viewpager);

        // setting up the adapter
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        // add the fragments
        viewPagerAdapter.add(new BrokerSplashoneFragment(), "Page 1");
        viewPagerAdapter.add(new BrokerSplashSecondFragment(), "Page 2");

        // Set the adapter
        viewPager.setAdapter(viewPagerAdapter);

        // The Page (fragment) titles will be displayed in the
        // tabLayout hence we need to  set the page viewer
        // we use the setupWithViewPager().
        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
    }
}
