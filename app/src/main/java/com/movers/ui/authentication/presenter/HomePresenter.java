package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.AddLoadJobPostFragment;
import com.movers.ui.authentication.fragment.AddTruckSpaceFragment;
import com.movers.ui.authentication.fragment.AutoShippersFragment;
import com.movers.ui.authentication.fragment.CompanyDetailFragment;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.MyNetworkFragment;
import com.movers.ui.authentication.fragment.MyRattingFragment;
import com.movers.ui.authentication.fragment.NotificationFragment;
import com.movers.ui.authentication.fragment.SingUpFragment;
import com.movers.ui.authentication.view.CompanyInfoView;
import com.movers.ui.authentication.view.HomeView;
import com.movers.utilsfile.Constant;

import javax.inject.Inject;

public class HomePresenter extends BasePresenter<HomeView> {

    String TAG = "CompanyInforPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;


    public void MyRattingFragment(FragmentActivity activity) {
        navigator.load(MyRattingFragment.class, false).replace(true);
    }

    public void MyNetworkFragment(FragmentActivity activity) {
        navigator. load(MyNetworkFragment.class, false).replace(true);
    }

    public void NotificationFragment(FragmentActivity activity) {
        navigator.load(NotificationFragment.class, false).replace(true);
    }

    public void AutoShippersFragment(FragmentActivity activity) {
        navigator.load(AutoShippersFragment.class, false).replace(true);
    }

    public void AddLoadJobPostFragment(FragmentActivity activity) {
        navigator.load(AddLoadJobPostFragment.class, false).replace(true);
    }

    @Inject
    public HomePresenter() {
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}