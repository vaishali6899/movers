package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.HomeActivity;
import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.NotificationFragment;
import com.movers.ui.authentication.fragment.SingUpFragment;
import com.movers.ui.authentication.view.ContactUsView;
import com.movers.ui.authentication.view.ShareView;

import javax.inject.Inject;

public class SharePresenter extends BasePresenter<ShareView> {

    String TAG = "CompanyInforPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;


    @Inject
    public SharePresenter() {
    }

    public void NotificationFragment(FragmentActivity activity) {
        navigator.load(NotificationFragment.class, false).replace(true);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}