package com.movers.ui.authentication.presenter;


import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.movers.HomeActivity;
import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.core.SubscribeWithView;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.NotificationFragment;
import com.movers.ui.authentication.fragment.SingUpFragment;
import com.movers.ui.authentication.model.register.RegisterResponse;
import com.movers.ui.authentication.view.CompanyInfoView;
import com.movers.ui.authentication.view.LoginView;

import java.util.HashMap;

import javax.inject.Inject;

public class CompanyInforPresenter extends BasePresenter<CompanyInfoView> {

    String TAG = "CompanyInforPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;

    public void SingUpFrangment(FragmentActivity activity) {
        navigator.load(SingUpFragment.class, false).replace(true);
    }


    public void HomeActivity(FragmentActivity activity) {
        navigator.loadActivity(HomeActivity.class).byFinishingAll().start();
    }
    @Inject
    public CompanyInforPresenter() {
    }

    public void NotificationFragment(FragmentActivity activity) {
        navigator.load(NotificationFragment.class, false).replace(true);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }
    public void registerApi(HashMap<String, String> hashMap) {
        view.showLoader();
        repository.register(hashMap)
                .subscribe(new SubscribeWithView<RegisterResponse>(view) {
                    @Override
                    public void onSuccess(RegisterResponse responseBody) {
                        view.hideLoader();
                        session.setLogin(true);
                        view.showMessage(responseBody.getSettings().getMessage());
                        Log.e(TAG, "onSuccess: "+ responseBody.getSettings().getMessage() );
                        navigator.loadActivity(HomeActivity.class).byFinishingAll().start();
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        view.hideLoader();
                        view.showMessage(e.getMessage());
                    }
                });
    }
}