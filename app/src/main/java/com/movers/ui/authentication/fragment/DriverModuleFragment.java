package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.adapter.MyDriverAdapter;
import com.movers.ui.authentication.adapter.MyNewtworkAdapter;
import com.movers.ui.authentication.model.driver.MyDriver;
import com.movers.ui.authentication.model.network.MyNetwork;
import com.movers.ui.authentication.presenter.AutoShippersPresenter;
import com.movers.ui.authentication.presenter.DriverModulePresenter;
import com.movers.ui.authentication.view.AutoShipperslView;
import com.movers.ui.authentication.view.DriverModuleView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;


public class DriverModuleFragment extends BaseFragment<DriverModulePresenter, DriverModuleView> implements DriverModuleView, MyDriverAdapter.OnItemClickListner {
    @BindView(R.id.tvTitle)
    public AppCompatTextView tvTitle;
    @BindView(R.id.imgNotification)
    AppCompatImageView imgNotification;

//    @BindView(R.id.DriverName)
//    AppCompatTextView DriverName;
//
//    @BindView(R.id.tvViewJobs)
//    AppCompatTextView tvViewJobs;
//    @BindView(R.id.tvRoute)
//    AppCompatTextView tvRoute;
//
//    @BindView(R.id.tvPhoneNumber)
//    AppCompatTextView tvPhoneNumber;
//
//    @BindView(R.id.tvEdit)
//    AppCompatButton tvEdit;
//
//    @BindView(R.id.tvPost)
//    AppCompatButton tvPost;
//    @BindView(R.id.tvDelete)
//    AppCompatButton tvDelete;

    @BindView(R.id.rvDriver)
    RecyclerView rvDriver;

    ArrayList<MyDriver> arrayList = new ArrayList<>();

    @Override
    protected int createLayout() {
        return R.layout.fragment_driver_module;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected DriverModuleView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        tvTitle.setText(getString(R.string.driver_module));
        MyDriver myNetwork = new MyDriver();
        myNetwork.setId("1");
        myNetwork.setCmp("Sinon");
        myNetwork.setBusiness("Sinon");
        myNetwork.setLogo("Sinon");
        myNetwork.setCmp("Sinon");
        arrayList.add(myNetwork);

        MyDriver myNetwork2 = new MyDriver();
        myNetwork2.setId("2");
        myNetwork2.setCmp("Sinon");
        myNetwork2.setBusiness("Sinon");
        myNetwork2.setLogo("Sinon");
        arrayList.add(myNetwork2);
        MyDriverAdapter myNewtworkAdapter = new MyDriverAdapter(getActivity(), arrayList, this);
        rvDriver.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvDriver.setAdapter(myNewtworkAdapter);
    }

    @OnClick({R.id.imgNotification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgNotification:
                presenter.NotificationFragment(getActivity());
                break;

        }
    }

    @Override
    public void OnItemClick(List<MyDriver> data, int position) {

    }
}