package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.core.Session;
import com.movers.data.Tags.UserTags;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.CompanyInforPresenter;
import com.movers.ui.authentication.view.CompanyInfoView;
import com.movers.utilsfile.Connection;
import com.movers.utilsfile.Constant;

import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;


public class CompanyInformationFragment extends BaseFragment<CompanyInforPresenter, CompanyInfoView> implements CompanyInfoView {

    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;
    @BindView(R.id.btnConfirm)
    AppCompatButton btnConfirm;

    @BindView(R.id.etCmpemailadd)
    AppCompatEditText etCmpemailadd;

    @BindView(R.id.etCmpMC)
    AppCompatEditText etCmpMC;
    @BindView(R.id.etCmpDot)
    AppCompatEditText etCmpDot;

    @BindView(R.id.etCmpCon)
    AppCompatEditText etCmpCon;

    @BindView(R.id.etCmpEmailAdd)
    AppCompatEditText etCmpEmailAdd;
    @BindView(R.id.etCmpName)
    AppCompatEditText etCmpName;
    String pwd;
    @Inject
    Session session;

    @Override
    protected int createLayout() {
        return R.layout.fragment_company_information;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected CompanyInfoView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        Bundle b = getArguments();
        pwd = b.getString("pwd");
    }


    @OnClick({R.id.ivBack, R.id.btnConfirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                presenter.SingUpFrangment(getActivity());
                break;

            case R.id.btnConfirm:
                if (etCmpName.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_compny_name));
                } else if (etCmpEmailAdd.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_compny_add));
                } else if (etCmpCon.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_compny_contact));
                } else if (etCmpDot.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_compny_dot));
                } else if (etCmpMC.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_compny_mc));
                } else if (etCmpemailadd.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_compny_email_add));
                } else {
                    if (Connection.isConnected(getActivity())) {
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put(UserTags.email, session.getEmail());
                        hashMap.put(UserTags.name, session.getFname());
                        hashMap.put(UserTags.last_name, session.getLastName());
                        hashMap.put(UserTags.password, pwd);
                        hashMap.put(UserTags.password_confirmation, pwd);
                        hashMap.put(UserTags.contact_number, session.getMobile());
                        hashMap.put(UserTags.type, Constant.UserType);
                        hashMap.put(UserTags.company_name, etCmpName.getText().toString());
                        hashMap.put(UserTags.cemail_address, etCmpEmailAdd.getText().toString());
                        hashMap.put(UserTags.ccontact_number, etCmpCon.getText().toString());
                        hashMap.put(UserTags.caddress, etCmpDot.getText().toString());
                        hashMap.put(UserTags.company_mc, etCmpMC.getText().toString());
                        hashMap.put(UserTags.company_dot, etCmpemailadd.getText().toString());
                        Log.e("TAG", "hashMap---------: " + new Gson().toJson(hashMap));
                        presenter.registerApi(hashMap);
                    } else {
                        showMessage(getResources().getString(R.string.no_internet_connecitons));
                    }
                }
                break;

        }
    }
}