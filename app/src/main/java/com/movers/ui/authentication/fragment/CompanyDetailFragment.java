package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.CompanyDetailPresenter;
import com.movers.ui.authentication.presenter.CompanyInforPresenter;
import com.movers.ui.authentication.view.CompanyDetailView;
import com.movers.ui.authentication.view.CompanyInfoView;

import butterknife.BindView;
import butterknife.OnClick;


public class CompanyDetailFragment extends BaseFragment<CompanyDetailPresenter, CompanyDetailView> implements CompanyDetailView {
    @BindView(R.id.imgBack)
    AppCompatImageView imgBack;
    @BindView(R.id.imgNotification)
    AppCompatImageView imgNotification;

    @Override
    protected int createLayout() {
        return R.layout.fragment_company_detail;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected CompanyDetailView createView() {
        return this;
    }

    @Override
    protected void bindData() {

    }

    @OnClick({R.id.imgBack, R.id.imgNotification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                presenter.HomeFrangment(getActivity());
                break;
            case R.id.imgNotification:
                presenter.NotificationFragment(getActivity());
                break;
        }
    }

}