package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.AboutUsPresenter;
import com.movers.ui.authentication.presenter.HomePresenter;
import com.movers.ui.authentication.view.AboutUsView;
import com.movers.ui.authentication.view.HomeView;

import butterknife.BindView;
import butterknife.OnClick;


public class AboutUsFragment extends BaseFragment<AboutUsPresenter, AboutUsView> implements AboutUsView {

    @BindView(R.id.imgNotification)
    public AppCompatImageView imgNotification;
    @BindView(R.id.tvTitle)
    public AppCompatTextView tvTitle;

    @Override
    protected int createLayout() {
        return R.layout.fragment_about_us;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected AboutUsView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        imgNotification.setVisibility(View.GONE);
        tvTitle.setText(R.string.about_us);
    }

    @OnClick({R.id.imgNotification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgNotification:
                presenter.NotificationFragment(getActivity());
                break;

        }
    }
}