package com.movers.ui.authentication.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.movers.R;
import com.movers.ui.authentication.fragment.DriverModuleFragment;
import com.movers.ui.authentication.fragment.MyNetworkFragment;
import com.movers.ui.authentication.model.driver.MyDriver;
import com.movers.ui.authentication.model.network.MyNetwork;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyDriverAdapter extends RecyclerView.Adapter<MyDriverAdapter.MyViewHolder> {
    private ArrayList<MyDriver> arrayCart;
    public Context mContext;
    public MyNetworkFragment activity;
    public MyDriverAdapter.OnItemClickListner onItemClickListner;
    String selectedItem = "";

    public MyDriverAdapter(FragmentActivity activity, ArrayList<MyDriver> arrayCart,
                           DriverModuleFragment cartFragment) {

        this.arrayCart = arrayCart;
        mContext = activity;
        onItemClickListner = (MyDriverAdapter.OnItemClickListner) cartFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_driver, parent, false);
        return new MyViewHolder(itemView);
    }

    public void setData(List<MyDriver> myarrayList) {
        this.arrayCart = new ArrayList<>(myarrayList);
        MyDriverAdapter.this.notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void OnItemClick(List<MyDriver> data, int position);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.e("TAG", "onBindViewHolder: "+  arrayCart.get(position).cmp);
        Log.e("TAG", "onBindViewHolder: "+  arrayCart.get(position).getCmp());
        holder.tvDriverName.setText(arrayCart.get(position).getCmp());
        holder.tvPhoneNumber.setText(arrayCart.get(position).getCmp());
        holder.tvViewJobs.setText(arrayCart.get(position).getCmp());
    }


    @Override
    public int getItemCount() {
        return arrayCart.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public Context context;

        @BindView(R.id.tvDriverName)
        AppCompatTextView tvDriverName;

        @BindView(R.id.tvViewJobs)
        AppCompatTextView tvViewJobs;
        @BindView(R.id.tvRoute)
        AppCompatTextView tvRoute;

        @BindView(R.id.tvPhoneNumber)
        AppCompatTextView tvPhoneNumber;

        @BindView(R.id.tvEdit)
        AppCompatButton tvEdit;

        @BindView(R.id.tvPost)
        AppCompatButton tvPost;
        @BindView(R.id.tvDelete)
        AppCompatButton tvDelete;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}