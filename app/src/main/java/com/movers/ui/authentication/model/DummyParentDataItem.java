package com.movers.ui.authentication.model;

import java.io.Serializable;
import java.util.ArrayList;

public class DummyParentDataItem implements Serializable {
    private String parentName;


    private ArrayList<DummyChildDataItem> childDataItems;

    public int getParentImage() {
        return parentImage;
    }

    public void setParentImage(int parentImage) {
        this.parentImage = parentImage;
    }

    private int parentImage;


    public DummyParentDataItem(String parentName, int img_home) {
        this.parentName = parentName;
        this.parentImage = img_home;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

}
