package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.BrokerSingupPresenter;
import com.movers.ui.authentication.presenter.BrokerSplashPresenter;
import com.movers.ui.authentication.view.BrokerSingupView;
import com.movers.ui.authentication.view.BrokerSplashView;

import butterknife.BindView;
import butterknife.OnClick;


public class BrokerSplashFragment extends BaseFragment<BrokerSplashPresenter, BrokerSplashView> implements BrokerSplashView {
    @BindView(R.id.btnGetStarted)
    public LinearLayout btnGetStarted;

    @Override
    protected int createLayout() {
        return R.layout.fragment_movers_splash;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected BrokerSplashView createView() {
        return this;
    }

    @Override
    protected void bindData() {

    }

    @OnClick({R.id.btnGetStarted})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnGetStarted:
                presenter.BrokerSingInFragment(getActivity());
                break;


        }
    }
}