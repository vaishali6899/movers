package com.movers.ui.authentication.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.movers.R;
import com.movers.ui.authentication.fragment.MyNetworkFragment;
import com.movers.ui.authentication.model.network.MyNetwork;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyNewtworkAdapter extends RecyclerView.Adapter<MyNewtworkAdapter.MyViewHolder> {
    private ArrayList<MyNetwork> arrayCart;
    public Context mContext;
    public MyNetworkFragment activity;
    public MyNewtworkAdapter.OnItemClickListner onItemClickListner;
    String selectedItem = "";

    public MyNewtworkAdapter(FragmentActivity activity, ArrayList<MyNetwork> arrayCart,
                             MyNetworkFragment cartFragment) {

        this.arrayCart = arrayCart;
        mContext = activity;
        onItemClickListner = (MyNewtworkAdapter.OnItemClickListner) cartFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_my_network, parent, false);
        return new MyViewHolder(itemView);
    }

    public void setData(List<MyNetwork> myarrayList) {
        this.arrayCart = new ArrayList<>(myarrayList);
        MyNewtworkAdapter.this.notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void OnItemClick(List<MyNetwork> data, int position);

        void onLikeUnlikeItemClick(List<MyNetwork> data, int position);

        void onChnagelikeItemClick(List<MyNetwork> data, int position, int id);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvCmp.setText("Cmp");
    }


    @Override
    public int getItemCount() {
        return arrayCart.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public Context context;

        @BindView(R.id.tvLogo)
        AppCompatTextView tvLogo;

        @BindView(R.id.tvCmp)
        AppCompatTextView tvCmp;

        @BindView(R.id.tvBusinessType)
        AppCompatTextView tvBusinessType;


        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}