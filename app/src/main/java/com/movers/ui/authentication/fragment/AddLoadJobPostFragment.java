package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.AddLoadJobPostPresenter;
import com.movers.ui.authentication.presenter.AddTruckSpacePresenter;
import com.movers.ui.authentication.view.AddLoadJobPostView;
import com.movers.ui.authentication.view.AddTruckSpaceView;

import butterknife.BindView;
import butterknife.OnClick;

public class AddLoadJobPostFragment extends BaseFragment<AddLoadJobPostPresenter, AddLoadJobPostView> implements AddLoadJobPostView {

    @BindView(R.id.imgBack)
    AppCompatImageView imgBack;

    @BindView(R.id.tvTitle)
    public AppCompatTextView tvTitle;
    @BindView(R.id.imgNotification)
    AppCompatImageView imgNotification;
    @BindView(R.id.rdPost)
    RadioButton rdPost;
    @BindView(R.id.rdDispatch)
    RadioButton rdDispatch;

    @BindView(R.id.rdCreate)
    RadioButton rdCreate;
    //origin
    @BindView(R.id.etOrCity)
    AppCompatEditText etOrCity;
    @BindView(R.id.etOrState)
    AppCompatEditText etOrState;

    @BindView(R.id.etOriginPostalCode)
    AppCompatEditText etOriginPostalCode;

    //Desitination Adresss
    @BindView(R.id.etDACity)
    AppCompatEditText etDACity;

    @BindView(R.id.etDAState)
    AppCompatEditText etDAState;

    @BindView(R.id.etDAPostalCode)
    AppCompatEditText etDAPostalCode;


    //Company Deatils
    @BindView(R.id.etCmpName)
    AppCompatEditText etCmpName;
    @BindView(R.id.etCmpContact)
    AppCompatEditText etCmpContact;
    @BindView(R.id.etCmpEmailAdd)
    AppCompatEditText etCmpEmailAdd;

    @BindView(R.id.rdCubic)
    RadioButton rdCubic;

    @BindView(R.id.rdPond)
    RadioButton rdPond;
    @BindView(R.id.etWeight)
    AppCompatEditText etWeight;

    @BindView(R.id.etdeliver)
    AppCompatEditText etdeliver;

    @BindView(R.id.etPickup)
    AppCompatEditText etPickup;

//    @BindView(R.id.etPickupDate)
//    AppCompatEditText etPickupDate;

    @BindView(R.id.etDeliverDate)
    AppCompatEditText etDeliverDate;

    @BindView(R.id.etCodAmt)
    AppCompatEditText etCodAmt;

    @BindView(R.id.etBalanceAmt)
    AppCompatEditText etBalanceAmt;


    @Override
    protected int createLayout() {
        return R.layout.fragment_add_load_job_post;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected AddLoadJobPostView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        tvTitle.setText(getString(R.string.add_loads_job));
    }

    @OnClick({R.id.imgNotification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgNotification:
                presenter.NotificationFragment(getActivity());
                break;
        }
    }
}