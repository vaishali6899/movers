package com.movers.ui.authentication.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.movers.HomeActivity;
import com.movers.R;
import com.movers.ui.authentication.model.DummyParentDataItem;


import java.util.ArrayList;

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.MyViewHolder> {
    private ArrayList<DummyParentDataItem> dummyParentDataItems;
    public DrawerItemClickListener listener;
    public Context appContext;

    public DrawerAdapter(HomeActivity homeActivity,
                         ArrayList<DummyParentDataItem> dummyParentDataItems, HomeActivity activity) {
        this.dummyParentDataItems = dummyParentDataItems;
        appContext = homeActivity;
        listener=activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_drawer, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        DummyParentDataItem dummyParentDataItem = dummyParentDataItems.get(position);
        holder.textView_parentName.setText(dummyParentDataItem.getParentName());
        Glide.with(appContext)
                .load(dummyParentDataItem.getParentImage())
                .centerCrop()
                .placeholder(R.drawable.img_placeholder)
                .into(holder.img_drawer);
    }

    @Override
    public int getItemCount() {
        return dummyParentDataItems.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public AppCompatTextView textView_parentName;
        public AppCompatImageView img_drawer;
        public AppCompatImageView img_dropdown;
        public RecyclerView recyclerViewHome;
        public Context context;

        MyViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            textView_parentName = itemView.findViewById(R.id.tv_parentName);
            img_drawer = itemView.findViewById(R.id.img_drawer);
            img_dropdown = itemView.findViewById(R.id.img_dropdown);
            recyclerViewHome = itemView.findViewById(R.id.recyclerViewHome);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    listener.onDrawerItem(getAdapterPosition());
                }
            });
        }
    }

    public interface DrawerItemClickListener
    {
        void onDrawerItem(int item);
    }
}