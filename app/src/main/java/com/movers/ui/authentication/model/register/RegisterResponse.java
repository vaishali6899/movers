package com.movers.ui.authentication.model.register;

import com.google.gson.annotations.SerializedName;
import com.movers.data.entity.ResponseBody;

public class RegisterResponse  extends  ResponseBody {

    @SerializedName("settings")
    private Settings settings;

    @SerializedName("data")
    private Data data;

    public Settings getSettings() {
        return settings;
    }

    public Data getData() {
        return data;
    }
}