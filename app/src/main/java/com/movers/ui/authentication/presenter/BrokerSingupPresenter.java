package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.HomeActivity;
import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.OTPVerifyFragment;
import com.movers.ui.authentication.view.AddLoadJobPostView;
import com.movers.ui.authentication.view.BrokerSingupView;

import javax.inject.Inject;

public class BrokerSingupPresenter extends BasePresenter<BrokerSingupView> {

    String TAG = "BrokerSingupPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;
    public  void OTPVerifyFragment(){
        navigator.load(OTPVerifyFragment.class, false).replace(true);
    }

    @Inject
    public BrokerSingupPresenter() {
    }

    public void HomeActivity(FragmentActivity activity) {
        navigator.loadActivity(HomeActivity.class).byFinishingAll().start();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}