package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.adapter.MyLoadJobAdapter;
import com.movers.ui.authentication.adapter.MyNewtworkAdapter;
import com.movers.ui.authentication.model.loadjob.MyLoadJob;
import com.movers.ui.authentication.model.network.MyNetwork;
import com.movers.ui.authentication.presenter.MyLoadJobPresenter;
import com.movers.ui.authentication.presenter.MyRattingPresenter;
import com.movers.ui.authentication.view.MyLoadJobView;
import com.movers.ui.authentication.view.MyRattingView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MyLoadJobFragment extends BaseFragment<MyLoadJobPresenter, MyLoadJobView> implements MyLoadJobView ,MyLoadJobAdapter.OnItemClickListner{
    @BindView(R.id.imgBack)
    AppCompatImageView imgBack;

    @BindView(R.id.tvTitle)
    public AppCompatTextView tvTitle;
    @BindView(R.id.imgNotification)
    AppCompatImageView imgNotification;

//    @BindView(R.id.etCity)
//    AppCompatEditText etCity;

//    @BindView(R.id.tvGo)
//    AppCompatTextView tvGo;
//    @BindView(R.id.tvShow1)
//    AppCompatEditText tvShow1;

//    @BindView(R.id.tvPickup)
//    AppCompatTextView tvPickup;
//
//    @BindView(R.id.tvPickup2)
//    AppCompatTextView tvPickup2;
//
//    @BindView(R.id.tvDelivery)
//    AppCompatTextView tvDelivery;
//
//    @BindView(R.id.tvDeliver2)
//    AppCompatTextView tvDeliver2;
//
//    @BindView(R.id.tvORderID)
//    AppCompatTextView tvORderID;
//    @BindView(R.id.tvOrderID2)
//    AppCompatTextView tvOrderID2;
//
//    @BindView(R.id.tvCmpPay)
//    AppCompatTextView tvCmpPay;
//
//    @BindView(R.id.cmp2)
//    AppCompatTextView cmp2;
//
//    @BindView(R.id.tvView)
//    AppCompatButton tvView;
//
//    @BindView(R.id.tvvtiew)
//    AppCompatButton tvvtiew;
//    @BindView(R.id.btnDelet)
//    AppCompatButton btnDelet;
//
//    @BindView(R.id.tvDelet2)
//    AppCompatButton tvDelet2;

    @BindView(R.id.rvMyLoad)
    RecyclerView rvMyLoad;
    ArrayList<MyLoadJob> arrayList = new ArrayList<>();
    @Override
    protected int createLayout() {
        return R.layout.fragment_my_load_job;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected MyLoadJobView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        tvTitle.setText(getString(R.string.add_loads_job));

        MyLoadJob myNetwork = new MyLoadJob();
        myNetwork.setId("1");
        myNetwork.setCmp("Sinon");
        myNetwork.setBusiness("Sinon");
        myNetwork.setLogo("Sinon");
        myNetwork.setCmp("Sinon");
        arrayList.add(myNetwork);

        MyLoadJob myNetwork2 = new MyLoadJob();
        myNetwork.setId("2");
        myNetwork.setCmp("Sinon");
        myNetwork.setBusiness("Sinon");
        myNetwork.setLogo("Sinon");
        myNetwork.setCmp("Sinon");
        arrayList.add(myNetwork2);

        MyLoadJobAdapter myNewtworkAdapter = new MyLoadJobAdapter(getActivity(), arrayList, this);
        rvMyLoad.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMyLoad.setAdapter(myNewtworkAdapter);

    }

    @Override
    public void OnItemClick(List<MyLoadJob> data, int position) {

    }

    @Override
    public void onLikeUnlikeItemClick(List<MyLoadJob> data, int position) {

    }

    @Override
    public void onChnagelikeItemClick(List<MyLoadJob> data, int position, int id) {

    }
}