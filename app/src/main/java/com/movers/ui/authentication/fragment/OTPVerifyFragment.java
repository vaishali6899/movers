package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.OtpVerifyPresenter;
import com.movers.ui.authentication.view.OTPVerifyView;

import butterknife.BindView;
import butterknife.OnClick;


public class OTPVerifyFragment extends BaseFragment<OtpVerifyPresenter, OTPVerifyView> implements OTPVerifyView {
    @BindView(R.id.btnSingUp)
    public AppCompatButton btnSingUp;

    @Override
    protected int createLayout() {
        return R.layout.fragment_o_t_p_verify;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected OTPVerifyView createView() {
        return this;
    }

    @Override
    protected void bindData() {

    }

    @OnClick({R.id.btnSingUp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSingUp:
                presenter.HomeF();
                break;


        }
    }
}