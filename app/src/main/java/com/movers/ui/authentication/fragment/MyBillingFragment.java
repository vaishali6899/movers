package com.movers.ui.authentication.fragment;

import android.app.Dialog;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.LoginPresenter;
import com.movers.ui.authentication.presenter.MyBillingPresenter;
import com.movers.ui.authentication.view.LoginView;
import com.movers.ui.authentication.view.MyBillingView;

import butterknife.BindView;
import butterknife.OnClick;


public class MyBillingFragment extends BaseFragment<MyBillingPresenter, MyBillingView> implements MyBillingView {
    @BindView(R.id.tvAddCard)
    AppCompatTextView tvAddCard;
    @BindView(R.id.tvTitle)
    public AppCompatTextView tvTitle;
    @BindView(R.id.imgNotification)
    public AppCompatImageView imgNotification;

    @Override
    protected int createLayout() {
        return R.layout.fragment_my_billing;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected MyBillingView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        tvTitle.setText(R.string.my_bill);
        imgNotification.setVisibility(View.GONE);
    }

    @OnClick({R.id.tvAddCard, R.id.imgNotification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvAddCard:
                openLikeDialoge();
                break;
            case R.id.imgNotification:
                presenter.NotificationFragment(getActivity());
                break;

        }
    }

    public void openLikeDialoge() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialoge_billingadd);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        AppCompatButton btnSearchCmp = dialog.findViewById(R.id.btnCmp);
        // AppCompatTextView txtNumber = dialog.findViewById(R.id.txtNumber);
        // txtTitle.setText("" + message);

        //AppCompatTextView txtok = dialog.findViewById(R.id.txtok);
        btnSearchCmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    protected boolean onBackActionPerform() {
        presenter.goBack();
        return super.onBackActionPerform();

    }
}