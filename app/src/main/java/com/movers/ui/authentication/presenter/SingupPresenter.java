package com.movers.ui.authentication.presenter;


import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import com.movers.HomeActivity;
import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.core.SubscribeWithView;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.CompanyInformationFragment;
import com.movers.ui.authentication.fragment.LoginFragment;
import com.movers.ui.authentication.model.register.RegisterResponse;
import com.movers.ui.authentication.view.SingupView;

import java.util.HashMap;

import javax.inject.Inject;

public class SingupPresenter extends BasePresenter<SingupView> {

    String TAG = "SingupPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;

    public void OpenCompnyInfo(FragmentActivity activity, String pwd) {
        Bundle bundle = new Bundle();
        bundle.putString("pwd", pwd);
        navigator.load(CompanyInformationFragment.class, false).setBundle(bundle).replace(true);
    }

    public void loginFrangment(FragmentActivity activity) {
        navigator.load(LoginFragment.class, false).replace(true);
    }


    public void HomeActivity(FragmentActivity activity) {
        navigator.loadActivity(HomeActivity.class).byFinishingAll().start();
    }

    @Inject
    public SingupPresenter() {

    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }


}