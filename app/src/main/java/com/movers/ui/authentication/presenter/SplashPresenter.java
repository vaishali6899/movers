package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.HomeActivity;
import com.movers.ui.authentication.fragment.LoginFragment;
import com.movers.ui.authentication.fragment.SelectUserFragment;
import com.movers.ui.authentication.view.SplashView;
import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;

import javax.inject.Inject;

public class SplashPresenter extends BasePresenter<SplashView> {

    String TAG = "LoginPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;


    public void Login(FragmentActivity activity) {
        navigator.load(LoginFragment.class, false).replace(true);
    }

    public void SelectUSerFragment(FragmentActivity activity) {
        navigator.load(SelectUserFragment.class, false).replace(true);
    }
    public void openDashboard(FragmentActivity activity) {
        navigator.loadActivity(HomeActivity.class).byFinishingAll().start();
    }
    @Inject
    public SplashPresenter() {
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}