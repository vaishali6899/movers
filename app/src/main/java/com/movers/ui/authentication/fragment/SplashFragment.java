package com.movers.ui.authentication.fragment;

import android.os.Handler;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.core.Session;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.SplashPresenter;
import com.movers.ui.authentication.view.SplashView;

import javax.inject.Inject;

public class SplashFragment extends BaseFragment<SplashPresenter, SplashView> implements SplashView {


    private Runnable runnable;
    Handler handler = new Handler();
    @Inject
    Session session;

    @Override
    protected int createLayout() {
        return R.layout.fragment_splash;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected SplashView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (session.getlogin()) {
                    presenter.openDashboard(getActivity());
                } else {
                    presenter.SelectUSerFragment(getActivity());
                }

            }
        }, 3000);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (runnable != null)
            handler.removeCallbacks(runnable);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (runnable != null)
            handler.postDelayed(runnable, 3000);
    }
}

