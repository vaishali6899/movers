package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.HomeActivity;
import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.NotificationFragment;
import com.movers.ui.authentication.fragment.SingUpFragment;
import com.movers.ui.authentication.view.AboutUsView;
import com.movers.ui.authentication.view.AutoShipperslView;

import javax.inject.Inject;

public class AboutUsPresenter extends BasePresenter<AboutUsView> {

    String TAG = "AboutUsPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;

    public void SingUpFrangment(FragmentActivity activity) {
        navigator.load(SingUpFragment.class, false).replace(false);
    }

    public void NotificationFragment(FragmentActivity activity) {
        navigator.load(NotificationFragment.class, false).replace(false);
    }

    public void HomeActivity(FragmentActivity activity) {
        navigator.loadActivity(HomeActivity.class).byFinishingAll().start();
    }
    @Inject
    public AboutUsPresenter() {
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}