package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.MyRattingPresenter;
import com.movers.ui.authentication.presenter.SelectUserPresenter;
import com.movers.ui.authentication.view.MyRattingView;
import com.movers.ui.authentication.view.SelectUserView;
import com.movers.utilsfile.Constant;

import butterknife.BindView;
import butterknife.OnClick;

public class SelectUserFragment extends BaseFragment<SelectUserPresenter, SelectUserView> implements SelectUserView {
    @BindView(R.id.placeHolder)
    public FrameLayout placeHolder;

    @BindView(R.id.llCarrier)
    public LinearLayout llCarrier;

    @BindView(R.id.llBroker)
    public LinearLayout llBroker;
    @BindView(R.id.llMovers)
    public LinearLayout llMovers;


    @BindView(R.id.llDriver)
    public LinearLayout llDriver;

    @BindView(R.id.btnContinue)
    public AppCompatTextView btnContinue;

    @Override
    protected int createLayout() {
        return R.layout.activity_select_user;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected SelectUserView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        llBroker.setAlpha(0.7f);
        llMovers.setAlpha(0.7f);
        llDriver.setAlpha(0.7f);

    }

    @OnClick({R.id.btnContinue, R.id.llCarrier, R.id.llBroker, R.id.llMovers, R.id.llDriver})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnContinue:
                if (Constant.UserType.equalsIgnoreCase("Movers")) {
                    presenter.Login(getActivity());
                } else if (Constant.UserType.equalsIgnoreCase("Carrier")) {
                    presenter.Login(getActivity());
                } else if (Constant.UserType.equalsIgnoreCase("Broker")) {
                    presenter.BrokerSingInFragment(getActivity());
                } else if (Constant.UserType.equalsIgnoreCase("Driver")) {
                    presenter.Login(getActivity());
                } else {
                    presenter.Login(getActivity());
                }
                break;

            case R.id.llCarrier:
                llCarrier.setAlpha(1);
                llBroker.setAlpha(0.7f);
                llMovers.setAlpha(0.7f);
                llDriver.setAlpha(0.7f);
                Constant.UserType = "carrier";
                break;

            case R.id.llBroker:
                llBroker.setAlpha(1);
                llCarrier.setAlpha(0.7f);
                llMovers.setAlpha(0.7f);
                llDriver.setAlpha(0.7f);
                Constant.UserType = "Broker";
                // presenter.BrokerSingInFragment(getActivity());
                //presenter.BrokerSplashActivity(getActivity());
                break;
            case R.id.llMovers:
                llMovers.setAlpha(1);
                llCarrier.setAlpha(0.7f);
                llBroker.setAlpha(0.7f);
                llDriver.setAlpha(0.7f);
                Constant.UserType = "Movers";
                // presenter.BrokerSingInFragment(getActivity());
                //presenter.BrokerSplashActivity(getActivity());
                break;

            case R.id.llDriver:
                llDriver.setAlpha(1);
                llCarrier.setAlpha(0.7f);
                llBroker.setAlpha(0.7f);
                llMovers.setAlpha(0.7f);
                Constant.UserType = "Driver";
                // presenter.BrokerSingInFragment(getActivity());
                //presenter.BrokerSplashActivity(getActivity());
                break;
        }
    }
}