package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.AutoShippersPresenter;
import com.movers.ui.authentication.presenter.ContactUsPresenter;
import com.movers.ui.authentication.view.AutoShipperslView;
import com.movers.ui.authentication.view.ContactUsView;

import butterknife.BindView;
import butterknife.OnClick;

public class ContactUsFragment extends BaseFragment<ContactUsPresenter, ContactUsView> implements ContactUsView {

    @BindView(R.id.imgNotification)
    public AppCompatImageView imgNotification;
    @BindView(R.id.tvTitle)
    public AppCompatTextView tvTitle;

    @Override
    protected int createLayout() {
        return R.layout.fragment_contact_us;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected ContactUsView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        imgNotification.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.contact_us));
    }

    @OnClick({R.id.imgNotification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgNotification:
                presenter.NotificationFragment(getActivity());
                break;

        }
    }
}