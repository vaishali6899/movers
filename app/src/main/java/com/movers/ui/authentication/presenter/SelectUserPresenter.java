package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.BrokerSplashActivity;
import com.movers.HomeActivity;
import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.BrokerSingInFragment;
import com.movers.ui.authentication.fragment.BrokerSplashFragment;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.LoginFragment;
import com.movers.ui.authentication.fragment.SingUpFragment;
import com.movers.ui.authentication.view.AboutUsView;
import com.movers.ui.authentication.view.SelectUserView;

import javax.inject.Inject;

public class SelectUserPresenter extends BasePresenter<SelectUserView> {

    String TAG = "AboutUsPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;


    public void Login(FragmentActivity activity) {
        navigator.load(LoginFragment.class, false).replace(true);
       // navigator.load(HomeFragment.class, false).replace(false);
    } public void BrokerSingInFragment(FragmentActivity activity) {
        navigator.load(BrokerSplashFragment.class, false).replace(true);
       // navigator.load(HomeFragment.class, false).replace(false);
    }

    public void HomeActivity(FragmentActivity activity) {
        navigator.loadActivity(HomeActivity.class).byFinishingAll().start();
    }  public void BrokerSplashActivity(FragmentActivity activity) {
        navigator.loadActivity(BrokerSplashActivity.class).byFinishingAll().start();
    }
    @Inject
    public SelectUserPresenter() {
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}