package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.NotificationFragment;
import com.movers.ui.authentication.fragment.SingUpFragment;
import com.movers.ui.authentication.view.CompanyDetailView;
import com.movers.ui.authentication.view.CompanyInfoView;

import javax.inject.Inject;

public class CompanyDetailPresenter extends BasePresenter<CompanyDetailView> {

    String TAG = "CompanyInforPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;
    public void NotificationFragment(FragmentActivity activity) {
        navigator.load(NotificationFragment.class, false).replace(true);
    }


    public void HomeFrangment(FragmentActivity activity) {
        navigator.load(HomeFragment.class, false).replace(true);
    }

    @Inject
    public CompanyDetailPresenter() {
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}