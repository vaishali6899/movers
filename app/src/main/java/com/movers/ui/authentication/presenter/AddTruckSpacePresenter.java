package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.HomeActivity;
import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.NotificationFragment;
import com.movers.ui.authentication.fragment.SingUpFragment;
import com.movers.ui.authentication.view.AboutUsView;
import com.movers.ui.authentication.view.AddTruckSpaceView;

import javax.inject.Inject;

public class AddTruckSpacePresenter extends BasePresenter<AddTruckSpaceView> {

    String TAG = "AddTruckSpacePresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;

    public void HomeF() {
        navigator.load(HomeFragment.class, false).replace(true);
    }

    public void NotificationFragment(FragmentActivity activity) {
        navigator.load(NotificationFragment.class, false).replace(true);
    }

    @Inject
    public AddTruckSpacePresenter() {
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}