package com.movers.ui.authentication.fragment;

import static com.google.android.gms.common.util.CollectionUtils.listOf;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.core.Session;
import com.movers.data.Tags.UserTags;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.LoginPresenter;
import com.movers.ui.authentication.presenter.SingupPresenter;
import com.movers.ui.authentication.view.LoginView;
import com.movers.ui.authentication.view.SingupView;
import com.movers.utilsfile.Connection;
import com.movers.utilsfile.Constant;
import com.movers.utilsfile.FileUtils;
import com.movers.utilsfile.HelperMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;


public class SingUpFragment extends BaseFragment<SingupPresenter, SingupView> implements SingupView {
    @BindView(R.id.btnSingUp)
    AppCompatButton btnSingUp;
    @BindView(R.id.ivBack)
    AppCompatImageView ivBack;

    @BindView(R.id.etFirstName)
    AppCompatEditText etFirstName;
    @BindView(R.id.etLastName)
    AppCompatEditText etLastName;

    @BindView(R.id.etMobileNo)
    AppCompatEditText etMobileNo;
    @BindView(R.id.etAddress)
    AppCompatEditText etAddress;
    @BindView(R.id.etpwd)
    AppCompatEditText etpwd;
    @BindView(R.id.etConfrimpwd)
    AppCompatEditText etConfrimpwd;

    @BindView(R.id.tvAccept)
    AppCompatTextView tvAccept;

    @BindView(R.id.ivFb)
    AppCompatImageView ivFb;

    @BindView(R.id.ivGmail)
    AppCompatImageView ivGmail;

    @BindView(R.id.tvrepwdShow)
    AppCompatTextView tvrepwdShow;

    @BindView(R.id.tvpwdShow)
    AppCompatTextView tvpwdShow;

    @BindView(R.id.llSingIn)
    LinearLayout llSingIn;


    public CallbackManager callbackManager;
    public int FB_SIGN_IN = 64206;
    String email = "", first_name = "", last_name = "", profile_pic = "", ID = "", strLang = "", loginType = "", TYPE = "";
    @Inject
    Session session;

    @Override
    protected int createLayout() {
        return R.layout.fragment_sing_up;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected SingupView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        DeviceTokenSetup();
        //initFacebook();
    }
//    private void initFacebook() {
//        LoginManager.getInstance().logOut();
//        callbackManager = CallbackManager.Factory.create();
//        login_button.setReadPermissions("public_profile", "email");
//        FacebookSdk.sdkInitialize(getContext().getApplicationContext());
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    private void DeviceTokenSetup() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                if (instanceIdResult.getToken() != null && !instanceIdResult.getToken().equals("")) {
                    session.setDeviceToken(instanceIdResult.getToken());
                } else {
                    session.setDeviceToken("dummy_token");
                }
            }
        });
    }

    @OnClick({R.id.btnSingUp, R.id.ivBack, R.id.tvpwdShow, R.id.tvrepwdShow, R.id.llSingIn, R.id.ivFb})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSingUp:
                if (etFirstName.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_first_name));
                } else if (etLastName.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_last_name));
                } else if (etMobileNo.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_mobile));
                } else if (etAddress.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_email_Address));
                } else if (!FileUtils.isEmailValid(etAddress.getText().toString())) {
                    showMessage(getString(R.string.plz_enter_email_valid_Address));
                } else if (etpwd.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_pwd));
                } else if (etConfrimpwd.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_confirm_pwd));
                } else {
                    session.setFirstName(etFirstName.getText().toString());
                    session.setLastName(etLastName.getText().toString());
                    session.setMobile(etMobileNo.getText().toString());
                    session.setEmail(etAddress.getText().toString());
                    presenter.OpenCompnyInfo(getActivity(), etpwd.getText().toString());
                }


                break;
            case R.id.ivBack:
                presenter.loginFrangment(getActivity());
                break;
            case R.id.tvpwdShow:
                if (tvpwdShow.getText().toString().equalsIgnoreCase("Hide")) {
                    tvpwdShow.setText(R.string.show);
                    etpwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    etpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    tvpwdShow.setText((R.string.hide));
                }
                break;
            case R.id.tvrepwdShow:
                if (tvrepwdShow.getText().toString().equalsIgnoreCase("Hide")) {
                    tvrepwdShow.setText((R.string.show));
                    etConfrimpwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    etConfrimpwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    tvrepwdShow.setText((R.string.hide));
                }
                break;

            case R.id.llSingIn:
                presenter.loginFrangment(getActivity());
                break;

            case R.id.ivFb:
                HelperMethod.hideSoftKeyboard(getActivity());
                facebookLogin();
                break;
        }
    }

    private void facebookLogin() {
        /*
         * For Facebook signup implementation
         * */
        LoginManager.getInstance()
                // .logInWithReadPermissions(this, listOf("public_profile", "email", "user_friends"));
                .logInWithReadPermissions(this, listOf("public_profile", "email"));
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance()
                .registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        getDataFromFacebook(loginResult);

                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Toast.makeText(getActivity(), "cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Toast.makeText(getActivity(), "" + exception, Toast.LENGTH_LONG).show();
                    }
                });
        try {
            @SuppressLint("PackageManagerGetSignatures")
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("fb_key_hash :", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    protected void getDataFromFacebook(LoginResult loginResult) {
        GraphRequest data_request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject json_object, GraphResponse response) {
                        try {
                            ID = json_object.getString("id");
                            first_name = json_object.getString("first_name");
                            last_name = json_object.getString("last_name");
                            JSONObject pic = json_object.getJSONObject("picture");
                            JSONObject img = pic.getJSONObject("data");
                            if (json_object.has("email")) {
                                email = json_object.getString("email");
                            }
                            profile_pic = img.getString("url");

                            session.setFirstName(first_name);
                            session.setSocialId(ID);
                            session.setLastName(last_name);
                            session.setEmail(email);
                            session.setLogin(true);
                            presenter.HomeActivity(getActivity());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,first_name,last_name,email,picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }

}