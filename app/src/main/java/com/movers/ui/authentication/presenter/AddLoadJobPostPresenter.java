package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.NotificationFragment;
import com.movers.ui.authentication.view.AddLoadJobPostView;
import com.movers.ui.authentication.view.AddTruckSpaceView;

import javax.inject.Inject;

public class AddLoadJobPostPresenter extends BasePresenter<AddLoadJobPostView> {

    String TAG = "AddTruckSpacePresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;
    public  void HomeF(){
        navigator.load(HomeFragment.class, false).replace(false);
    }

    @Inject
    public AddLoadJobPostPresenter() {
    }

    public void NotificationFragment(FragmentActivity activity) {
        navigator.load(NotificationFragment.class, false).replace(true);
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}