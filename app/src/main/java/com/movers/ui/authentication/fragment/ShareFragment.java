package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.SharePresenter;
import com.movers.ui.authentication.view.ShareView;

import butterknife.BindView;
import butterknife.OnClick;


public class ShareFragment extends BaseFragment<SharePresenter, ShareView> implements ShareView {

    @BindView(R.id.tvTitle)
    public AppCompatTextView tvTitle;

    @BindView(R.id.btnshare)
    public AppCompatButton btnshare;
    @BindView(R.id.imgNotification)
    public AppCompatImageView imgNotification;

    @Override
    protected int createLayout() {
        return R.layout.fragment_share;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected ShareView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        tvTitle.setText(getString(R.string.share));
    }

    @OnClick({R.id.btnshare, R.id.imgNotification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnshare:
                break;
            case R.id.imgNotification:
                presenter.NotificationFragment(getActivity());
                break;
        }
    }

}