package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.AddLoadJobPostPresenter;
import com.movers.ui.authentication.presenter.MyProfilePresenter;
import com.movers.ui.authentication.view.AddLoadJobPostView;
import com.movers.ui.authentication.view.MyProfileView;

import butterknife.BindView;
import butterknife.OnClick;


public class MyProfileFragment extends BaseFragment<MyProfilePresenter, MyProfileView> implements MyProfileView {
    @BindView(R.id.imgBack)
    AppCompatImageView imgBack;

    @BindView(R.id.tvTitle)
    public AppCompatTextView tvTitle;
    @BindView(R.id.imgNotification)
    AppCompatImageView imgNotification;

    @BindView(R.id.tvDispatch)
    AppCompatTextView tvDispatch;

    @BindView(R.id.tvName)
    AppCompatTextView tvName;
    @BindView(R.id.tvPwd)
    AppCompatTextView tvPwd;

    @BindView(R.id.tvUserType)
    AppCompatTextView tvUserType;
    @BindView(R.id.tvRatting)
    AppCompatTextView tvRatting;

    @BindView(R.id.tvExternalPage)
    AppCompatTextView tvExternalPage;

    @BindView(R.id.tvOwnerConName)
    AppCompatTextView tvOwnerConName;
    @BindView(R.id.tvConName)
    AppCompatTextView tvConName;
    @BindView(R.id.tvWebsitecmp)
    AppCompatTextView tvWebsitecmp;

    @Override
    protected int createLayout() {
        return R.layout.fragment_my_profile;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected MyProfileView createView() {
        return null;
    }

    @Override
    protected void bindData() {
        imgBack.setBackgroundDrawable(getActivity().getDrawable(R.drawable.ic_back));
        tvTitle.setText(R.string.my_profile);
    }

    @OnClick({R.id.imgBack, R.id.imgNotification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                presenter.HomeF();
                break;
            case R.id.imgNotification:
                presenter.NotificationFragment(getActivity());
                break;
        }
    }
}