package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.MyRattingPresenter;
import com.movers.ui.authentication.presenter.NotificationPresenter;
import com.movers.ui.authentication.view.MyRattingView;
import com.movers.ui.authentication.view.NotificationView;

import butterknife.BindView;


public class NotificationFragment extends BaseFragment<NotificationPresenter, NotificationView> implements NotificationView {
    @BindView(R.id.imgNotification)
    public AppCompatImageView imgNotification;
    @BindView(R.id.tvTitle)
    public AppCompatTextView tvTitle;
    @BindView(R.id.rvNotify)
    public RecyclerView rvNotify;

    @Override
    protected int createLayout() {
        return R.layout.fragment_notification;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected NotificationView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        imgNotification.setVisibility(View.GONE);
        tvTitle.setText(R.string.notification);
    }
}