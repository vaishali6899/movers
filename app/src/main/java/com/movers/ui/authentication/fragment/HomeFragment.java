package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.CompanyInforPresenter;
import com.movers.ui.authentication.presenter.HomePresenter;
import com.movers.ui.authentication.view.CompanyInfoView;
import com.movers.ui.authentication.view.HomeView;

import butterknife.BindView;
import butterknife.OnClick;


public class HomeFragment extends BaseFragment<HomePresenter, HomeView> implements HomeView {
    @BindView(R.id.llAverageRate)
    public LinearLayout llAverageRate;
    @BindView(R.id.llMynetworks)
    public LinearLayout llMynetworks;
    @BindView(R.id.llMyloadJob)
    public LinearLayout llMyloadJob;
    @BindView(R.id.llDispatchReq)
    public LinearLayout llDispatchReq;

    @BindView(R.id.llAddLoadJob)
    public LinearLayout llAddLoadJob;
    @BindView(R.id.ivmenu)
    public AppCompatImageView ivmenu;

    @BindView(R.id.imgNotification)
    public AppCompatImageView imgNotification;

    @Override
    protected int createLayout() {
        return R.layout.fragment_home;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected HomeView createView() {
        return this;
    }

    @Override
    protected void bindData() {

    }

    @OnClick({R.id.llAverageRate
            , R.id.llMynetworks, R.id.ivmenu, R.id.llAddLoadJob, R.id.imgNotification
            , R.id.llDispatchReq})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.llAverageRate:
                presenter.MyRattingFragment(getActivity());
                break;
            case R.id.ivmenu:
                presenter.toggleDrawer();
                break;

            case R.id.llMynetworks:
                presenter.MyNetworkFragment(getActivity());
                break;

            case R.id.llAddLoadJob:
                presenter.AddLoadJobPostFragment(getActivity());
                break;


            case R.id.imgNotification:
                presenter.NotificationFragment(getActivity());
                break;

            case R.id.llDispatchReq:
                presenter.AutoShippersFragment(getActivity());
                break;


        }
    }
}