package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.HomeActivity;
import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.NotificationFragment;
import com.movers.ui.authentication.fragment.SingUpFragment;
import com.movers.ui.authentication.view.AutoShipperslView;
import com.movers.ui.authentication.view.CompanyInfoView;

import javax.inject.Inject;

public class AutoShippersPresenter extends BasePresenter<AutoShipperslView> {

    String TAG = "CompanyInforPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;

    public void HomeFragment(FragmentActivity activity) {
        navigator.load(HomeFragment.class, false).replace(false);
    }

    @Inject
    public AutoShippersPresenter() {
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}