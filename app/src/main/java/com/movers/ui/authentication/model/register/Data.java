package com.movers.ui.authentication.model.register;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("token")
	private String token;

	public String getToken(){
		return token;
	}
}