package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.AddTruckSpacePresenter;
import com.movers.ui.authentication.presenter.AutoShippersPresenter;
import com.movers.ui.authentication.view.AddTruckSpaceView;
import com.movers.ui.authentication.view.AutoShipperslView;

import butterknife.BindView;
import butterknife.OnClick;


public class AddTruckSpaceFragment extends BaseFragment<AddTruckSpacePresenter, AddTruckSpaceView> implements AddTruckSpaceView {
    @BindView(R.id.imgBack)
    AppCompatImageView imgBack;

    @BindView(R.id.imgNotification)
    AppCompatImageView imgNotification;

    @BindView(R.id.tvTitle)
    public AppCompatTextView tvTitle;

    @BindView(R.id.etORCity)
    public AppCompatEditText etORCity;
    @BindView(R.id.etORState)
    public AppCompatEditText etORState;

    @BindView(R.id.etORPostalCode)
    public AppCompatEditText etORPostalCode;

    @BindView(R.id.etRtCity)
    public AppCompatEditText etRtCity;
    @BindView(R.id.etRtState)
    public AppCompatEditText etRtState;

    @BindView(R.id.etRTPostalCode)
    public AppCompatEditText etRTPostalCode;

    @BindView(R.id.etDACity)
    public AppCompatEditText etDACity;

    @BindView(R.id.etDAState)
    public AppCompatEditText etDAState;

    @BindView(R.id.etDAPostalCode)
    public AppCompatEditText etDAPostalCode;

    @BindView(R.id.etTotalPrice)
    public AppCompatEditText etTotalPrice;

    @BindView(R.id.etContactInfo)
    public AppCompatEditText etContactInfo;

    @BindView(R.id.etContactNumber)
    public AppCompatEditText etContactNumber;

    @BindView(R.id.etWeigt)
    public AppCompatEditText etWeigt;


    @Override
    protected int createLayout() {
        return R.layout.fragment_add_truck_space;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected AddTruckSpaceView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        imgBack.setBackgroundDrawable(getActivity().getDrawable(R.drawable.ic_back));
        tvTitle.setText(R.string.add_truck_space);
    }

    @OnClick({R.id.imgBack, R.id.imgNotification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                presenter.HomeF();
                break;
            case R.id.imgNotification:
                presenter.NotificationFragment(getActivity());
                break;
        }
    }

    @Override
    protected boolean onBackActionPerform() {
        presenter.goBack();
        return super.onBackActionPerform();

    }
}