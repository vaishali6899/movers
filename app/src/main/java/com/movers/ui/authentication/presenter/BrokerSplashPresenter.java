package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.HomeActivity;
import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.BrokerSingInFragment;
import com.movers.ui.authentication.fragment.BrokerSingUpFragment;
import com.movers.ui.authentication.fragment.NotificationFragment;
import com.movers.ui.authentication.fragment.SingUpFragment;
import com.movers.ui.authentication.view.AboutUsView;
import com.movers.ui.authentication.view.BrokerSplashView;

import javax.inject.Inject;

public class BrokerSplashPresenter extends BasePresenter<BrokerSplashView> {

    String TAG = "AboutUsPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;

    public void BrokerSingInFragment(FragmentActivity activity) {
        navigator.load(BrokerSingInFragment.class, false).replace(true);
    }

    public void NotificationFragment(FragmentActivity activity) {
        navigator.load(NotificationFragment.class, false).replace(true);
    }

    public void HomeActivity(FragmentActivity activity) {
        navigator.loadActivity(HomeActivity.class).byFinishingAll().start();
    }

    @Inject
    public BrokerSplashPresenter() {
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}