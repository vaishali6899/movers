package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.HomeActivity;
import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.BrokerSingInFragment;
import com.movers.ui.authentication.fragment.BrokerSingUpFragment;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.view.AddLoadJobPostView;
import com.movers.ui.authentication.view.BrokerSingInView;

import javax.inject.Inject;

public class BrokerSingInPresenter extends BasePresenter<BrokerSingInView> {

    String TAG = "BrokerSingInPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;


    @Inject
    public BrokerSingInPresenter() {
    }
    public void HomeActivity(FragmentActivity activity) {
        navigator.loadActivity(HomeActivity.class).byFinishingAll().start();
    }

    public void BrokerSingUpFragment(FragmentActivity activity) {
        navigator.load(BrokerSingUpFragment.class, false).replace(true);
        // navigator.load(HomeFragment.class, false).replace(false);
    }
    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}