package com.movers.ui.authentication.fragment;

import static com.google.android.gms.common.util.CollectionUtils.listOf;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.os.SystemClock;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.Login;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.core.Session;
import com.movers.data.Tags.UserTags;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.LoginPresenter;
import com.movers.ui.authentication.presenter.SplashPresenter;
import com.movers.ui.authentication.view.LoginView;
import com.movers.ui.authentication.view.SplashView;
import com.movers.utilsfile.AppLog;
import com.movers.utilsfile.Connection;
import com.movers.utilsfile.Connectivity;
import com.movers.utilsfile.Constant;
import com.movers.utilsfile.HelperMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;


public class LoginFragment extends BaseFragment<LoginPresenter, LoginView> implements LoginView, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.btnSingUp)
    public AppCompatButton btnSingUp;
    @BindView(R.id.etMobileNumber)
    public AppCompatEditText etMobileNumber;

    @BindView(R.id.etPwd)
    public AppCompatEditText etPwd;
    @BindView(R.id.ivFacebook)
    public AppCompatImageView ivFacebook;
    @BindView(R.id.ivGmail)
    public AppCompatImageView ivGmail;
    @BindView(R.id.tvSingUp)
    public AppCompatTextView tvSingUp;
    @BindView(R.id.tvForegotpwd)
    public AppCompatTextView tvForegotpwd;
    @BindView(R.id.tvShow)
    public AppCompatTextView tvShow;

    @Inject
    Session session;

    @BindView(R.id.llSingUp)
    public LinearLayout llSingUp;
    public GoogleApiClient googleApiClient;
    public final int GOOGLE_SIGN_IN = 100;
    public boolean GoogleClientConnected = true;
    public CallbackManager callbackManager;
    public int FB_SIGN_IN = 64206;
    //facebook

    String email = "", first_name = "", last_name = "", profile_pic = "", ID = "", strLang = "", loginType = "", TYPE = "";


    // facebook login
    @BindView(R.id.login_button)
    public LoginButton login_button;


    @Override
    protected int createLayout() {
        return R.layout.fragment_login;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected LoginView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        DeviceTokenSetup();
        initFacebook();
    }

    private void FacebookIntegration() {
        if (Connectivity.isConnected(getActivity())) {
            login_button.performClick();
            login_button.setPressed(true);
            login_button.invalidate();
            login_button.registerCallback(callbackManager, mCallBack);
            login_button.setPressed(false);
            login_button.invalidate();
        } else {
            showMessage(getResources().getString(R.string.no_internet_connecitons));
        }
    }

    // Facebook
    private void initFacebook() {
        LoginManager.getInstance().logOut();
        callbackManager = CallbackManager.Factory.create();
        login_button.setReadPermissions("public_profile", "email");
        FacebookSdk.sdkInitialize(getContext().getApplicationContext());
    }


    @OnClick({R.id.btnSingUp, R.id.ivGmail, R.id.tvShow, R.id.tvSingUp, R.id.llSingUp, R.id.ivFacebook})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSingUp:
                if (etMobileNumber.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_mobile));
                } else if (etPwd.getText().toString().isEmpty()) {
                    showMessage(getString(R.string.plz_enter_pwd));
                } else {
                    if (Connection.isConnected(getActivity())) {
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put(UserTags.email, etMobileNumber.getText().toString());
                        hashMap.put(UserTags.password, etPwd.getText().toString());
                        hashMap.put(UserTags.user_type, Constant.UserType);
                        Log.e("TAG", "hashMap---------: " + new Gson().toJson(hashMap));
                        presenter.login(hashMap);
                    } else {
                        showMessage(getResources().getString(R.string.no_internet_connecitons));
                    }
                }
                break;

            case R.id.tvShow:
                if (tvShow.getText().toString().equalsIgnoreCase("Hide")) {
                    tvShow.setText(R.string.show);
                    etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    tvShow.setText((R.string.hide));
                }
                break;

            case R.id.llSingUp:
            case R.id.tvSingUp:
                presenter.SingUpFrangment(getActivity());
                break;


            case R.id.ivFacebook:
                HelperMethod.hideSoftKeyboard(getActivity());
//                LoginManager.getInstance().logOut();
//                session.clearSession();
                facebookLogin();
                break;
            case R.id.ivGmail:
                signInWithGmail();
                break;

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void DeviceTokenSetup() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                if (instanceIdResult.getToken() != null && !instanceIdResult.getToken().equals("")) {
                    session.setDeviceToken(instanceIdResult.getToken());
                } else {
                    session.setDeviceToken("dummy_token");
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }


    }

    public void signInWithGmail() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        Constant.mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
        Intent signInIntent = Constant.mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 123);
    }

    //Relate to google login
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                String id = account.getId();
                String email = account.getEmail();
                String fname = "" + account.getGivenName();
                String lname = "" + account.getFamilyName();

                String pic_url;
                if (account.getPhotoUrl() != null) {
                    pic_url = account.getPhotoUrl().toString();
                } else {
                    pic_url = "null";
                }

                presenter.HomeActivity(getActivity());

                session.setFirstName(fname);
                session.setLastName(lname);
                session.setSocialId(id);
                session.setEmail(email);
                session.setLogin(true);
            }
        } catch (ApiException e) {
            Log.w("Error message", "signInResult:failed code=" + e.getStatusCode());
        }
    }


    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.e("TAG", "loginResult: " + loginResult);
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            try {
                                String facebookid = object.getString("id").toString();
                                String email, mobile_phone;
                                if (object.has("email")) {
                                    email = object.getString("email").toString();
                                } else {
                                    email = "";
                                }
                                String name = object.getString("name").toString();
                                String id = object.getString("id").toString();
                                int firstSpaceIndex = name.indexOf(" ");
                                String firstName = name.substring(0, firstSpaceIndex);
                                String lastname = name.substring(firstSpaceIndex + 1);
                                //  RegisterWithFacebook(firstName, email, Constant.login_social_f, facebookid, lastname);
                                Toast.makeText(getActivity(), getResources().getString(R.string.login_success), Toast.LENGTH_SHORT).show();
                                session.setFirstName(firstName);
                                session.setLastName(lastname);
                                session.setSocialId(id);
                                session.setEmail(email);
                                session.setLogin(true);
                                Log.e("Exception", "Exception" + email);
                                Log.e("Exception", "Exception" + email);
                                presenter.HomeActivity(getActivity());

                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("Exception", "Exception" + e.toString());
                                FacebookIntegration();
                            }
                        }
                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)");
            request.setParameters(parameters);
            request.executeAsync();
        }


        @Override
        public void onCancel() {
            AccessToken token = AccessToken.getCurrentAccessToken();
            if (token != null) {
                GraphRequest request = GraphRequest.newMeRequest(
                        token,
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                JSONObject responseJSONObject = response.getJSONObject();
                                AppLog.Log("TAG", "Appplication login=" + responseJSONObject.toString());
                                Log.e("TAG", "Appplication" + responseJSONObject.toString());

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }
        }

        @Override
        public void onError(FacebookException e) {
            Log.e("TAG", "Appplication login=" + e.toString());
        }
    };


    private void facebookLogin() {
        /*
         * For Facebook signup implementation
         * */
        LoginManager.getInstance()
                // .logInWithReadPermissions(this, listOf("public_profile", "email", "user_friends"));
                .logInWithReadPermissions(this, listOf("public_profile", "email"));
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance()
                .registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        getDataFromFacebook(loginResult);

                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Toast.makeText(getActivity(), "cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Toast.makeText(getActivity(), "" + exception, Toast.LENGTH_LONG).show();
                    }
                });
        try {
            @SuppressLint("PackageManagerGetSignatures")
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("fb_key_hash :", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    protected void getDataFromFacebook(LoginResult loginResult) {
        GraphRequest data_request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject json_object, GraphResponse response) {
                        try {
                            ID = json_object.getString("id");
                            first_name = json_object.getString("first_name");
                            last_name = json_object.getString("last_name");
                            JSONObject pic = json_object.getJSONObject("picture");
                            JSONObject img = pic.getJSONObject("data");
                            if (json_object.has("email")) {
                                email = json_object.getString("email");
                            }
                            profile_pic = img.getString("url");

                            session.setFirstName(first_name);
                            session.setSocialId(ID);
                            session.setLastName(last_name);
                            session.setEmail(email);
                            session.setLogin(true);
                            presenter.HomeActivity(getActivity());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,first_name,last_name,email,picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }
}