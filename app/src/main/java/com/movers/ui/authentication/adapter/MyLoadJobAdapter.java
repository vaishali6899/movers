package com.movers.ui.authentication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.movers.R;
import com.movers.ui.authentication.fragment.MyNetworkFragment;
import com.movers.ui.authentication.model.loadjob.MyLoadJob;
import com.movers.ui.authentication.model.network.MyNetwork;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyLoadJobAdapter extends RecyclerView.Adapter<MyLoadJobAdapter.MyViewHolder> {
    private ArrayList<MyLoadJob> arrayCart;
    public Context mContext;
    public MyNetworkFragment activity;
    public MyLoadJobAdapter.OnItemClickListner onItemClickListner;
    String selectedItem = "";

    public MyLoadJobAdapter(FragmentActivity activity, ArrayList<MyLoadJob> arrayCart,
                            Fragment cartFragment) {

        this.arrayCart = arrayCart;
        mContext = activity;
        onItemClickListner = (MyLoadJobAdapter.OnItemClickListner) cartFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_load_job, parent, false);
        return new MyViewHolder(itemView);
    }

    public void setData(List<MyLoadJob> myarrayList) {
        this.arrayCart = new ArrayList<>(myarrayList);
        MyLoadJobAdapter.this.notifyDataSetChanged();
    }

    public interface OnItemClickListner {
        void OnItemClick(List<MyLoadJob> data, int position);

        void onLikeUnlikeItemClick(List<MyLoadJob> data, int position);

        void onChnagelikeItemClick(List<MyLoadJob> data, int position, int id);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvDelivery.setText("Cmp");

    }


    @Override
    public int getItemCount() {
        return arrayCart.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public Context context;


        @BindView(R.id.tvPickup)
        AppCompatTextView tvPickup;

        @BindView(R.id.tvDelivery)
        AppCompatTextView tvDelivery;

        @BindView(R.id.tvORderID)
        AppCompatTextView tvORderID;

        @BindView(R.id.tvCmpPay)
        AppCompatTextView tvCmpPay;


        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}