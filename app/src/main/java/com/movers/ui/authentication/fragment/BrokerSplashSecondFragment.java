package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.BrokerSplashPresenter;
import com.movers.ui.authentication.view.BrokerSplashView;

import butterknife.BindView;
import butterknife.OnClick;


public class BrokerSplashSecondFragment extends BaseFragment<BrokerSplashPresenter, BrokerSplashView> implements BrokerSplashView {


    @BindView(R.id.llGetStarted)
    public LinearLayout llGetStarted;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_broker_splash_second, container, false);
    }

    @Override
    protected int createLayout() {
        return R.layout.fragment_broker_splash_second;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected BrokerSplashView createView() {
        return this;
    }

    @Override
    protected void bindData() {

    }

    @OnClick({R.id.llGetStarted})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llGetStarted:
                presenter.BrokerSingInFragment(getActivity());
                break;
        }
    }
}