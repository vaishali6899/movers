package com.movers.ui.authentication.presenter;


import androidx.fragment.app.FragmentActivity;

import com.movers.HomeActivity;
import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.fragment.NotificationFragment;
import com.movers.ui.authentication.fragment.SingUpFragment;
import com.movers.ui.authentication.view.AutoShipperslView;
import com.movers.ui.authentication.view.ContactUsView;

import javax.inject.Inject;

public class ContactUsPresenter extends BasePresenter<ContactUsView> {

    String TAG = "CompanyInforPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;
    public void NotificationFragment(FragmentActivity activity) {
        navigator.load(NotificationFragment.class, false).replace(true);
    }


    @Inject
    public ContactUsPresenter() {
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}