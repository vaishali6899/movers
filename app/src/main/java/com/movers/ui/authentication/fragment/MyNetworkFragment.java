package com.movers.ui.authentication.fragment;

import android.app.Dialog;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.adapter.MyNewtworkAdapter;
import com.movers.ui.authentication.model.network.MyNetwork;
import com.movers.ui.authentication.presenter.MyNetworkPresenter;
import com.movers.ui.authentication.presenter.SplashPresenter;
import com.movers.ui.authentication.view.MyNetworkView;
import com.movers.ui.authentication.view.SplashView;
import com.movers.utilsfile.AppLog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class MyNetworkFragment extends BaseFragment<MyNetworkPresenter, MyNetworkView> implements MyNetworkView, MyNewtworkAdapter.OnItemClickListner {
    @BindView(R.id.tvTitle)
    AppCompatTextView tvTitle;
    @BindView(R.id.btnCmp)
    AppCompatButton btnCmp;
    @BindView(R.id.imgNotification)
    AppCompatImageView imgNotification;

    @BindView(R.id.btnLoadMoreUser)
    AppCompatButton btnLoadMoreUser;


    @BindView(R.id.rvMyNetwork)
    RecyclerView rvMyNetwork;

    @BindView(R.id.tvPreferred)
    AppCompatTextView tvPreferred;

    @BindView(R.id.tvBlockList)
    AppCompatTextView tvBlockList;

    ArrayList<MyNetwork> arrayList = new ArrayList<>();

    @Override
    protected int createLayout() {
        return R.layout.fragment_my_network;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected MyNetworkView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        tvTitle.setText(R.string.my_network);

        MyNetwork myNetwork = new MyNetwork();
        myNetwork.setId("1");
        myNetwork.setCmp("Sinon");
        myNetwork.setBusiness("Sinon");
        myNetwork.setLogo("Sinon");
        myNetwork.setCmp("Sinon");
        arrayList.add(myNetwork);

        MyNetwork myNetwork2 = new MyNetwork();
        myNetwork.setId("2");
        myNetwork.setCmp("Sinon");
        myNetwork.setBusiness("Sinon");
        myNetwork.setLogo("Sinon");
        myNetwork.setCmp("Sinon");
        arrayList.add(myNetwork2);
        MyNewtworkAdapter myNewtworkAdapter = new MyNewtworkAdapter(getActivity(), arrayList, this);
        rvMyNetwork.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMyNetwork.setAdapter(myNewtworkAdapter);
    }

    @OnClick({R.id.btnCmp, R.id.imgNotification,R.id.tvBlockList,R.id.tvPreferred})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCmp:
                openLikeDialoge();
                break;
            case R.id.imgNotification:
                presenter.NotificationFragment(getActivity());
                break;


            case R.id.tvBlockList:
                tvPreferred.setAlpha(0.7f);
                tvBlockList.setAlpha(1);
                break;

            case R.id.tvPreferred:
                tvPreferred.setAlpha(1);
                tvBlockList.setAlpha(0.7f);
                break;
        }
    }

    public void openLikeDialoge() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialoge_addcompany);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        AppCompatButton btnSearchCmp = dialog.findViewById(R.id.btnSearchCmp);
        // AppCompatTextView txtNumber = dialog.findViewById(R.id.txtNumber);
        // txtTitle.setText("" + message);

        //AppCompatTextView txtok = dialog.findViewById(R.id.txtok);
        btnSearchCmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void OnItemClick(List<MyNetwork> data, int position) {
        Log.e("TAG", "OnItemClick: " + new Gson().toJson(data));
    }

    @Override
    public void onLikeUnlikeItemClick(List<MyNetwork> data, int position) {

    }

    @Override
    public void onChnagelikeItemClick(List<MyNetwork> data, int position, int id) {

    }
}