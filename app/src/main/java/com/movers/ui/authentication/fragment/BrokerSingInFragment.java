package com.movers.ui.authentication.fragment;

import static com.google.android.gms.common.util.CollectionUtils.listOf;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.core.Session;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.BrokerSingInPresenter;
import com.movers.ui.authentication.view.BrokerSingInView;
import com.movers.utilsfile.AppLog;
import com.movers.utilsfile.Connectivity;
import com.movers.utilsfile.HelperMethod;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class BrokerSingInFragment extends BaseFragment<BrokerSingInPresenter, BrokerSingInView> implements BrokerSingInView {
    @BindView(R.id.btnSingUp)
    public AppCompatButton btnSingUp;

    @BindView(R.id.llRegis)
    public LinearLayout llRegis;
    @BindView(R.id.etFirstName)
    public AppCompatEditText etFirstName;
    @BindView(R.id.etpwd)
    public AppCompatEditText etpwd;
    @BindView(R.id.llFaceBok)
    public LinearLayout llFaceBok;
    @BindView(R.id.login_button)
    public LoginButton login_button;
    public CallbackManager callbackManager;
    public int FB_SIGN_IN = 64206;
    @Inject
    Session session;
    String email = "", first_name = "", last_name = "", profile_pic = "", ID = "", strLang = "", loginType = "", TYPE = "";

    @Override
    protected int createLayout() {
        return R.layout.fragment_broker_sing_in;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected BrokerSingInView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        DeviceTokenSetup();
        initFacebook();
    }

    private void DeviceTokenSetup() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                if (instanceIdResult.getToken() != null && !instanceIdResult.getToken().equals("")) {
                    session.setDeviceToken(instanceIdResult.getToken());
                } else {
                    session.setDeviceToken("dummy_token");
                }
            }
        });
    }

    @OnClick({R.id.btnSingUp, R.id.llFaceBok})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSingUp:
                presenter.BrokerSingUpFragment(getActivity());
                break;
            case R.id.llFaceBok:
                HelperMethod.hideSoftKeyboard(getActivity());
                facebookLogin();
                break;

        }
    }

    private void FacebookIntegration() {
        if (Connectivity.isConnected(getActivity())) {
            login_button.performClick();
            login_button.setPressed(true);
            login_button.invalidate();
            login_button.registerCallback(callbackManager, mCallBack);
            login_button.setPressed(false);
            login_button.invalidate();
        } else {
            showMessage(getResources().getString(R.string.no_internet_connecitons));
        }
    }

    // Facebook
    private void initFacebook() {
        LoginManager.getInstance().logOut();
        callbackManager = CallbackManager.Factory.create();
        login_button.setReadPermissions("public_profile", "email");
        FacebookSdk.sdkInitialize(getContext().getApplicationContext());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);


    }

    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            try {
                                String facebookid = object.getString("id").toString();
                                String email, mobile_phone;
                                if (object.has("email")) {
                                    email = object.getString("email").toString();
                                } else {
                                    email = "";
                                }
                                String name = object.getString("name").toString();
                                String id = object.getString("id").toString();
                                int firstSpaceIndex = name.indexOf(" ");
                                Toast.makeText(getActivity(), getResources().getString(R.string.login_success), Toast.LENGTH_SHORT).show();
                                String firstName = name.substring(0, firstSpaceIndex);
                                String lastname = name.substring(firstSpaceIndex + 1);
                                //  RegisterWithFacebook(firstName, email, Constant.login_social_f, facebookid, lastname);
                                session.setFirstName(firstName);
                                session.setLastName(lastname);
                                session.setSocialId(id);
                                session.setEmail(email);
                                session.setLogin(true);
                                presenter.HomeActivity(getActivity());

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)");
            request.setParameters(parameters);
            request.executeAsync();
        }


        @Override
        public void onCancel() {
            AccessToken token = AccessToken.getCurrentAccessToken();
            if (token != null) {
                GraphRequest request = GraphRequest.newMeRequest(
                        token,
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                JSONObject responseJSONObject = response.getJSONObject();
                                AppLog.Log("TAG", "Appplication login=" + responseJSONObject.toString());

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }
        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    private void facebookLogin() {
        /*
         * For Facebook signup implementation
         * */
        LoginManager.getInstance()
                // .logInWithReadPermissions(this, listOf("public_profile", "email", "user_friends"));
                .logInWithReadPermissions(this, listOf("public_profile", "email"));
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance()
                .registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        getDataFromFacebook(loginResult);

                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Toast.makeText(getActivity(), "cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Toast.makeText(getActivity(), "" + exception, Toast.LENGTH_LONG).show();
                    }
                });
        try {
            @SuppressLint("PackageManagerGetSignatures")
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("fb_key_hash :", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    protected void getDataFromFacebook(LoginResult loginResult) {
        GraphRequest data_request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject json_object, GraphResponse response) {
                        try {
                            ID = json_object.getString("id");
                            first_name = json_object.getString("first_name");
                            last_name = json_object.getString("last_name");
                            JSONObject pic = json_object.getJSONObject("picture");
                            JSONObject img = pic.getJSONObject("data");
                            if (json_object.has("email")) {
                                email = json_object.getString("email");
                            }
                            profile_pic = img.getString("url");

                            session.setFirstName(first_name);
                            session.setSocialId(ID);
                            session.setLastName(last_name);
                            session.setEmail(email);
                            session.setLogin(true);
                            presenter.HomeActivity(getActivity());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,first_name,last_name,email,picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }

}