package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.AutoShippersPresenter;
import com.movers.ui.authentication.view.AutoShipperslView;

import butterknife.BindView;
import butterknife.OnClick;


public class AutoShippersFragment extends BaseFragment<AutoShippersPresenter, AutoShipperslView> implements AutoShipperslView {
    @BindView(R.id.imgBack)
    public AppCompatImageView imgBack;

    @BindView(R.id.tvNumber)
    public AppCompatTextView tvNumber;

    @BindView(R.id.tvOwnerName)
    public AppCompatTextView tvOwnerName;
    @BindView(R.id.tvBusinessType)
    public AppCompatTextView tvBusinessType;

    @BindView(R.id.tvHours)
    public AppCompatTextView tvHours;
    @BindView(R.id.tvEstablished)
    public AppCompatTextView tvEstablished;

    @BindView(R.id.tvContact)
    public AppCompatTextView tvContact;
    @BindView(R.id.tvMainPhone)
    public AppCompatTextView tvMainPhone;

    @BindView(R.id.tvLocalPhone)
    public AppCompatTextView tvLocalPhone;
    @BindView(R.id.tvFAXNumber)
    public AppCompatTextView tvFAXNumber;

    @BindView(R.id.tvEmail)
    public AppCompatTextView tvEmail;


    @Override
    protected int createLayout() {
        return R.layout.fragment_auto_shippers;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected AutoShipperslView createView() {
        return this;
    }

    @Override
    protected void bindData() {

    }

    @OnClick({R.id.imgBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                presenter.HomeFragment(getActivity());
                break;
        }
    }

//    @Override
//    protected boolean onBackActionPerform() {
//        Log.e("TAG", "onBackActionPerform: " );
//        presenter.goBack();
//        return super.onBackActionPerform();
//
//    }
}