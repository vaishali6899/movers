package com.movers.ui.authentication.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.movers.R;
import com.movers.base.BaseFragment;
import com.movers.di.component.FragmentComponent;
import com.movers.ui.authentication.presenter.DriverModulePresenter;
import com.movers.ui.authentication.presenter.MyRattingPresenter;
import com.movers.ui.authentication.view.DriverModuleView;
import com.movers.ui.authentication.view.MyRattingView;

import butterknife.BindView;
import butterknife.OnClick;


public class MyRattingFragment extends BaseFragment<MyRattingPresenter, MyRattingView> implements MyRattingView {

    @BindView(R.id.imgNotification)
    public AppCompatImageView imgNotification;
    @BindView(R.id.tvTitle)
    public AppCompatTextView tvTitle;
    @BindView(R.id.tvPositive)
    public AppCompatTextView tvPositive;
    @BindView(R.id.tvnatural)
    public AppCompatTextView tvnatural;

    @BindView(R.id.tvnegative)
    public AppCompatTextView tvnegative;

    @Override
    protected int createLayout() {
        return R.layout.fragment_my_ratting;
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected MyRattingView createView() {
        return this;
    }

    @Override
    protected void bindData() {
        imgNotification.setVisibility(View.GONE);
        tvTitle.setText(getString(R.string.my_rat));
    }

    @OnClick({R.id.imgBack, R.id.imgNotification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                presenter.goBack();
                break;
            case R.id.imgNotification:
                presenter.NotificationFragment(getActivity());
                break;
        }
    }


    // TODO: Rename parameter arguments, choose names that match

}