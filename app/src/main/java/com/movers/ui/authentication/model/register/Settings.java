package com.movers.ui.authentication.model.register;

import com.google.gson.annotations.SerializedName;

public class Settings{

	@SerializedName("success")
	private int success;

	@SerializedName("message")
	private String message;

	public int getSuccess(){
		return success;
	}

	public String getMessage(){
		return message;
	}
}