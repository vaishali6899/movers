package com.movers.ui.authentication.presenter;


import com.movers.base.BasePresenter;
import com.movers.core.Session;
import com.movers.data.repository.UserRepository;
import com.movers.ui.authentication.fragment.HomeFragment;
import com.movers.ui.authentication.view.AddLoadJobPostView;
import com.movers.ui.authentication.view.OTPVerifyView;

import javax.inject.Inject;

public class OtpVerifyPresenter extends BasePresenter<OTPVerifyView> {

    String TAG = "OtpVerifyPresenter";
    @Inject
    Session session;

    @Inject
    UserRepository repository;

    public void HomeF() {
        navigator.load(HomeFragment.class, false).replace(true);
    }

    @Inject
    public OtpVerifyPresenter() {
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

}