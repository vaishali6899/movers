package com.movers.exception;

/**
 * Created by hlink21 on 6/1/17.
 */

public class ErrorCode {
    public static final int LOGOUT = 401;
    public static final int REQUIRED_SIGN_UP = 406;
    public static final int OTP_VERIFICATION = 412;
}
