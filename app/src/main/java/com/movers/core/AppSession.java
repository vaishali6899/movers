package com.movers.core;

import android.content.Context;


import com.twitter.sdk.android.core.models.User;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by hlink21 on 11/7/16.
 */
@Singleton
public class AppSession implements Session {

    private final String apiKey;
    private AppPreferences appPreferences;
    private Context context;
    private User user;
    private String language;


    @Inject
    public AppSession(AppPreferences appPreferences, Context context, @Named("api-key") String apiKey) {
        this.appPreferences = appPreferences;
        this.context = context;
        this.apiKey = apiKey;
        language = "english";
    }

    @Override
    public String getRegisterType() {
        return appPreferences.getString(REGISTER_TYPE);
    }

    @Override
    public String getTemp_profile() {
        return appPreferences.getString(temp_profile);
    }

    @Override
    public boolean isOTP_Verify() {
        return appPreferences.getBoolean(OTPTEXT);
    }

    @Override
    public boolean isMinistry_add() {
        return appPreferences.getBoolean(MINISTRY_ADD);
    }

    @Override
    public boolean isEmail_Verify() {
        return appPreferences.getBoolean(EMAILVERIFY);
    }

    @Override
    public String getMobileOtp() {
        return appPreferences.getString(MOBILE_OTP);
    }

    @Override
    public String getEmailOtp() {
        return appPreferences.getString(EMAIL_OTP);
    }

    @Override
    public String getCounty_code() {
        return appPreferences.getString(County_code);
    }

    @Override
    public String getSocialId() {
        return appPreferences.getString(SOCIAL_ID);
    }

    @Override
    public String getZipcode() {
        return appPreferences.getString(ZIPCODE);
    }

    @Override
    public String getZender() {
        return appPreferences.getString(ZENDER);
    }

    @Override
    public String getDeviceType() {
        return appPreferences.getString(DEVICETYPE);
    }

    @Override
    public String getDeviceToken() {
        return appPreferences.getString(DEVICE_TOKEN);
    }

    @Override
    public String getApiKey() {
        return apiKey;
    }

    @Override
    public String getMinistry() {
        return appPreferences.getString(MINISTRY);
    }

    @Override
    public String getUserSession() {
        return appPreferences.getString(USER_SESSION);
    }

    @Override
    public String getUserId() {
        return appPreferences.getString(USER_ID);
    }

    @Override
    public void setApiKey(String apiKey) {
    }

    @Override
    public void setUserSession(String userSession) {
        appPreferences.putString(USER_SESSION, userSession);
    }


    @Override
    public void setUserId(String userId) {
        appPreferences.putString(USER_ID, userId);
    }

    @Override
    public void setMinistry(String ministry) {
        appPreferences.putString(MINISTRY, ministry);
    }

    @Override
    public void setCounty_code(String county_code) {
        appPreferences.putString(County_code, county_code);
    }

    @Override
    public String getDeviceId() {
        String token = "";
        /* open below comment after Firebase integration */

        /*FirebaseInstanceId.getInstance().getToken();
        if (StringUtils.isEmpty(token))
            token = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);*/

        return token;
    }

    @Override
    public void clearSession() {
        appPreferences.clearAll();
    }

    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public void setLogin(Boolean login) {
        appPreferences.putBoolean(LOGIN, login);

    }

    @Override
    public Boolean getlogin() {
        return appPreferences.getBoolean(LOGIN);
    }

    @Override
    public void setLastName(String name) {
        appPreferences.putString(LNAME, name);

    }

    @Override
    public void setFirstName(String name) {
        appPreferences.putString(FNAME, name);

    }

    @Override
    public void setSocialId(String socialId) {
        appPreferences.putString(SOCIAL_ID, socialId);
    }

    @Override
    public void setTemp_profile(String temp_profile) {
        appPreferences.putString(temp_profile, temp_profile);
    }

    @Override
    public void setOTP_Verify(boolean OTP_Verify) {
        appPreferences.putBoolean(OTPTEXT, OTP_Verify);
    }

    @Override
    public void setEmail_Verify(boolean email_Verify) {
        appPreferences.putBoolean(EMAILVERIFY, email_Verify);
    }

    @Override
    public void setMinistry_add(boolean ministry_add) {
        appPreferences.putBoolean(MINISTRY_ADD, ministry_add);
    }

    @Override
    public String getMobile() {
        return appPreferences.getString(MOBILE);
    }

    @Override
    public void setMobile(String mobile) {
        appPreferences.putString(MOBILE, mobile);

    }

    @Override
    public String getLastName() {
        return appPreferences.getString(LNAME);
    }

    @Override
    public String getFname() {
        return appPreferences.getString(FNAME);
    }

    @Override
    public void setStudent_id(String student_id) {
        appPreferences.putString(STUDENT_ID, student_id);

    }

    @Override
    public void setMobileOtp(String mobileOtp) {
        appPreferences.putString(MOBILE_OTP, mobileOtp);
    }

    @Override
    public void setEmailOtp(String emailOtp) {
        appPreferences.putString(EMAIL_OTP, emailOtp);
    }

    @Override
    public void setDeviceToken(String deviceToken) {
        appPreferences.putString(DEVICE_TOKEN, deviceToken);
    }

    @Override
    public String getStudent_id() {
        return appPreferences.getString(STUDENT_ID);
    }

    @Override
    public void setEmail(String email) {
        appPreferences.putString(EMAIL, email);
    }

    @Override
    public String getEmail() {
        return appPreferences.getString(EMAIL);
    }

    @Override
    public void setProfile(String profile) {
        appPreferences.putString(PROFILE, profile);
    }

    @Override
    public void setDeviceType(String deviceType) {
        appPreferences.putString(DEVICETYPE, deviceType);
    }

    @Override
    public void setRegisterType(String registerType) {
        appPreferences.putString(REGISTER_TYPE, registerType);
    }

    @Override
    public void setZipcode(String zipcode) {
        appPreferences.putString(ZIPCODE, zipcode);
    }

    @Override
    public void setZender(String zender) {
        appPreferences.putString(ZENDER, zender);
    }

    @Override
    public String getProfile() {
        return appPreferences.getString(PROFILE);
    }


    @Override
    public String getCURRENT_UPDATED_LOCATION_LATITUDE() {
        return appPreferences.getString(CURRENT_UPDATED_LOCATION_LATITUDE);
    }

    @Override
    public String getCURRENT_UPDATED_LOCATION_LONGITUDE() {
        return appPreferences.getString(CURRENT_UPDATED_LOCATION_LONGITUDE);

    }


    @Override
    public void setCURRENT_UPDATED_LOCATION_LATITUDE(String lattitude) {
        appPreferences.putString(CURRENT_UPDATED_LOCATION_LATITUDE, lattitude);
    }

    @Override
    public void setCURRENT_UPDATED_LOCATION_LONGITUDE(String longitude) {
        appPreferences.putString(CURRENT_UPDATED_LOCATION_LONGITUDE, longitude);
    }

}
