package com.movers.core;

import java.io.IOException;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * Created by hlink21 on 29/11/17.
 */
@Singleton
public class AESCryptoInterceptor implements Interceptor {

    private final AES aes;

    @Inject
    public AESCryptoInterceptor(AES aes) {
        this.aes = aes;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        final Request request = chain.request();

        Set<String> plainQueryParameters = request.url().queryParameterNames();
        HttpUrl httpUrl = request.url();
        // Check Query Parameters and encrypt
        if (plainQueryParameters != null && !plainQueryParameters.isEmpty()) {
            HttpUrl.Builder httpUrlBuilder = httpUrl.newBuilder();
            for (int i = 0; i < plainQueryParameters.size(); i++) {
                String name = httpUrl.queryParameterName(i);
                String value = httpUrl.queryParameterValue(i);
                httpUrlBuilder.setQueryParameter(name, aes.encrypt(value));
            }
            httpUrl = httpUrlBuilder.build();
        }

        // Get Header for encryption
        String apiKey = request.headers().get(Session.API_KEY);
        String token = request.headers().get(Session.USER_SESSION);
        Request newRequest;
        Request.Builder requestBuilder = request.newBuilder();

        // Check if any body and encrypt
        RequestBody requestBody = request.body();
        if (requestBody != null && requestBody.contentType() != null) {
            // bypass multipart parameters for encryption
            boolean isMultipart = !requestBody.contentType().type().equalsIgnoreCase("multipart");
            byte[] bodyPlainText = isMultipart ? transformInputStream(bodyToString(requestBody)) : bodyToString(requestBody);

            if (bodyPlainText != null) {
                requestBuilder
                        .post(RequestBody.create(
                                requestBody.contentType(),
                                bodyPlainText)
                        );
            }
        }
        // Build the final request
        newRequest = requestBuilder.url(httpUrl)
                .header(Session.API_KEY, aes.encrypt(apiKey))
                .header(Session.USER_SESSION, aes.encrypt(token))
                .build();


        // execute the request
        Response proceed = chain.proceed(newRequest);
        // get the response body and decrypt it.
        String cipherBody = proceed.body().string();
        String plainBody = aes.decrypt(cipherBody);

        // create new CMSDatam with plaint text body for further process
        return proceed.newBuilder()
                .body(ResponseBody.create(MediaType.parse("text/json"), plainBody.trim()))
                .build();

    }

    private byte[] bodyToString(final RequestBody request) {
        try {
            final Buffer buffer = new Buffer();
            if (request != null)
                request.writeTo(buffer);
            else
                return null;
            return buffer.readByteArray();
        } catch (final IOException e) {
            return null;
        }
    }

    private byte[] transformInputStream(byte[] inputStream) {
        return aes.encrypt(inputStream);
    }
}
