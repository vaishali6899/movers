package com.movers.core;




import com.movers.base.RootView;

import java.lang.ref.WeakReference;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

/**
 * Created by hlink21 on 21/7/16.
 */
public abstract class SubscribeWithView<T> implements SingleObserver<T>, Observer<T> {

    private WeakReference<RootView> rootView;


    public SubscribeWithView(RootView rootView) {
        this.rootView = new WeakReference<>(rootView);
    }

    @Override
    public void onSubscribe(Disposable d) {
        if (rootView.get() != null)
            rootView.get().addDisposable(d);

    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
        if (rootView.get() != null)
            rootView.get().onError(e);
    }

    @Override
    public void onNext(T t) {
        onSuccess(t);
    }

    @Override
    public void onComplete() {

    }
}
