package com.movers.core;


/**
 * Created by hlink21 on 11/7/16.
 */
public interface Session {

    String API_KEY = "API-KEY";
    String USER_SESSION = "TOKEN";
    String USER_ID = "USER_ID";
    String DEVICE_TYPE = "A";
    String LOGIN = "false";
    String LNAME = "Lname";
    String FNAME = "fname";
    String EMAIL = "email";
    String MOBILE = "mobile";
    String STUDENT_ID = "stu_id";
    String PROFILE = "profile";
    String MINISTRY = "ministry";
    String DEVICE_TOKEN = "device_token";
    String ZENDER = "zender";
    String DEVICETYPE= "device_type";
    String REGISTER_TYPE= "register_type";
    String ZIPCODE= "zipcode";
    String SOCIAL_ID= "social_id";
    String OTPTEXT= "otp_verify";
    String EMAILVERIFY= "email_verify";
    String MOBILE_OTP= "mobile_otp";
    String EMAIL_OTP= "email_otp";
    String MINISTRY_ADD= "minsitry_add";
    String County_code="country_code";
    String temp_profile="temp_profile";
    boolean OTP_Verify=false;
    boolean Email_Verify=false;
    boolean ministry_add=false;
    String CURRENT_UPDATED_LOCATION_LATITUDE = "current_updatedt_latitude";
    String CURRENT_UPDATED_LOCATION_LONGITUDE = "current_updatedt_longitude";



    String getRegisterType();

    String getTemp_profile();

    boolean isOTP_Verify();

    boolean isMinistry_add();

    boolean isEmail_Verify();


    String getMobileOtp();

    String getEmailOtp();

    String getCounty_code();

    String getSocialId();

    String getZipcode();

    String getZender();

    String getDeviceType();

    String getDeviceToken();

    String getApiKey();

    String getMinistry();

    String getUserSession();

    String getUserId();

    void setApiKey(String apiKey);

    void setUserSession(String userSession);

    void setUserId(String userId);

    void setMinistry(String ministry);

    void setCounty_code(String county_code);

    String getDeviceId();

    void clearSession();

    String getLanguage();

    void setLogin(Boolean login);

    Boolean getlogin();

    void setLastName(String name);
    void setFirstName(String name);

    void setSocialId(String socialId);

    void setTemp_profile(String temp_profile);

    void setOTP_Verify(boolean OTP_Verify);

    void setEmail_Verify(boolean email_Verify);

    void setMinistry_add(boolean ministry_add);

    String getMobile();

    void setMobile(String mobile);

    String getLastName();
    String getFname();

    void setStudent_id(String student_id);

    void setMobileOtp(String mobileOtp);

    void setEmailOtp(String emailOtp);

    void setDeviceToken(String deviceToken);

    String getStudent_id();

    void setEmail(String email);

    String getEmail();

    void setProfile(String profile);

    void setDeviceType(String deviceType);

    void setRegisterType(String registerType);

    void setZipcode(String zipcode);

    void setZender(String zender);

    String getProfile();


    void setCURRENT_UPDATED_LOCATION_LATITUDE(String latitude);
    String getCURRENT_UPDATED_LOCATION_LATITUDE();

    void setCURRENT_UPDATED_LOCATION_LONGITUDE(String longitude);
    String getCURRENT_UPDATED_LOCATION_LONGITUDE();

}
