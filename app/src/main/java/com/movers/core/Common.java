package com.movers.core;

import android.Manifest;

import java.io.File;

/**
 * Created by hlink21 on 27/4/16.
 */
public final class Common {

    public static final String IMAGE_URI = "image_uri";
    public static final String VIDEO_URI = "video_uri";
    public static final String SHARED_PREF_NAME = "application";
    public static final String ACTIVITY_FIRST_PAGE = "FirstPage";
    public static final String CURRENT_USER = "current_user";
    public static final String USER_BUNDLE = "UserBundle";
    public static final String USER_JSON = "UserJSON";
    public static final String LANGUAGE_PREF_NAME = "imagesPatientLanguage";
    public static final String IsEnglish="isEnglishLanguage";



    public static final String APP_DIRECTORY = "YSP";
    public static final String SNAP_DIRECTORY = APP_DIRECTORY + File.separator + "Snap";

    public static final int REQUEST_PERMISSION = 1;
    public static final int REQUEST_CAMERA_PERMISSION = 1;
    public static final int REQUEST_GALLERY_PERMISSION = 2;

    public static final String[] PERMISSIONS_GALLERY = {Manifest.permission.READ_EXTERNAL_STORAGE};

    public static final String[] PERMISSIONS_CAMERA = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};

    public static final String[] PERMISSIONS_RECORD_VIDEO = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};

    public static final String IS_LOGIN = "is_login";

    public static final int TAP_TO_LOAD_LIMIT = 4;
    public static final int STORYBOARD_LOAD_LIMIT = 7;
    public static final String MESSAGE = "message";
    public static final String DISTANCE = "Distance";
    public static final String THEME_COLOR = "theme_color";
    public static final int CAMERA_OPEN_REQUEST = 1002;
    public static final int GALLERY_OPEN_REQUEST = 1003;
    public static final int CAMERA_REQUEST = 101;
    public static final int STORAGE_REQUEST = 102;
    public static final int ADDRESS_REQUEST = 103;
    public static final int REQUEST_PERMISSION_CALL = 104;

    public static String IS_ON_SERVICE = "is_on_service";


    public static class RequestCode {
        public static final int REQUEST_TAKE_PHOTO = 1;
        public static final int RESULT_LOAD_IMAGE = 2;
        public static final int REQUEST_IMAGE_AND_VIDEO = 3;
        public static final int REQUEST_FROM_CAMERA = 4;
        public static final int REQUEST_TO_FINISH = 5;
        public static final int REQUEST = 10;
        public static final int REQUEST_TRIM_VIDEO = 11;
        public static final int CROP_IMAGE_ACTIVITY_REQUEST_CODE = 203;

    }

    public static class ResultCode {
        public static final int IMAGE_RESULT = 1;
        public static final int VIDEO_RESULT = 2;
        public static final int FINISH_ME = 444;
        public static final int MESSAGE = 10;

    }

    public static class RegX {
        public static final String USERNAME = "^[A-Za-z0-9_-]{3,25}$";
        public static final String FULL_NAME = "^(?!\\s)$";

    }

    public enum SelectionModes {
        NONE, SINGLE, MULTI
    }

}
