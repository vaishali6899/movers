package com.movers;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;
import androidx.core.app.ActivityCompat;
import android.text.TextUtils;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.movers.utilsfile.AppController;
import com.movers.di.Injector;


public class Movers extends Application
{

    //client account
    private static final String TWITTER_KEY ="KccY5MsFdpufagIuKXC0YmK4z";
    private static final String TWITTER_SECRET ="kySJfUzTtzKfw6p1yqQXUMDhIAraTRW701JwR98TrvMglQ6F0F";

    private boolean isNightModeEnabled = false;
    public static final String NIGHT_MODE = "NIGHT_MODE";
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
//        FirebaseApp.initializeApp(this);
//        FirebaseMessaging.getInstance().setAutoInitEnabled(true);

        Injector.INSTANCE.initAppComponent(this, "");
       // TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);

       // TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
       // Fabric.with(this, new Twitter(authConfig));

       // Fabric.with(this, new Twitter(authConfig));
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.isNightModeEnabled = mPrefs.getBoolean(NIGHT_MODE, false);
       // AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public boolean isNightModeEnabled() {
        return isNightModeEnabled;
    }


    public void setIsNightModeEnabled(boolean isNightModeEnabled) {
        this.isNightModeEnabled = isNightModeEnabled;

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean(NIGHT_MODE, isNightModeEnabled);
        editor.apply();
    }

    public static final String TAG = AppController.class.getSimpleName();
    private RequestQueue mRequestQueue;

    private static Movers mInstance;
    BroadcastReceiver receiver;


    public static synchronized Movers getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        req.setRetryPolicy(new DefaultRetryPolicy(60000 * 30,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context mContext) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isAllowed(Context mInstance) {
        if (ActivityCompat.checkSelfPermission(mInstance, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //added multidex to handle 64k memory issues.
        MultiDex.install(this);
    }
}