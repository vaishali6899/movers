package com.movers.common.dialog;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;


import com.movers.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Vishwa on 14/7/18.
 */

public class TwoButtonDialog extends DialogFragment {

    public String message;
    public TwoButtonListener listener;
    @BindView(R.id.buttonYes)
    AppCompatTextView buttonYes;
    @BindView(R.id.buttonNo)
    AppCompatTextView buttonNo;
    @BindView(R.id.txt_meseg)
    AppCompatTextView txt_meseg;
    @Nullable

    public String str_message;

    public TwoButtonDialog(String message) {
        str_message = message;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_two_button, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);
        getDialog().setCancelable(false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        txt_meseg.setText(str_message);
    }


    @OnClick({R.id.buttonYes, R.id.buttonNo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonYes:
                listener.onYes();
                dismiss();
                break;
            case R.id.buttonNo:
                listener.onNo();
                dismiss();
                break;
        }
    }

    public interface TwoButtonListener {
        void onYes();

        void onNo();
    }
}
