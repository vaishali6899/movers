package com.movers.data.datasource;


import android.text.TextUtils;

import com.movers.exception.SuccessException;
import com.movers.core.AppExecutors;
import com.movers.data.entity.ResponseBody;
import com.movers.exception.ServerException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


/**
 * Created by hlink21 on 3/1/17.
 */

public class BaseDataSource {

    private final AppExecutors appExecutors;

    public BaseDataSource(AppExecutors appExecutors) {
        this.appExecutors = appExecutors;
    }


    protected <T extends ResponseBody> Single<T> execute(Single<T> observable) {
        return observable
                .subscribeOn(Schedulers.from(appExecutors.networkIO()))
                .observeOn(Schedulers.from(appExecutors.mainThread()))
                .doOnSuccess(t -> {
                    if (!t.status.equalsIgnoreCase("1")) {
                        if (t.responseCode.equalsIgnoreCase("200")) {
                            throw new SuccessException(t.message, t.responseCode);
                        } else {
                            throw new ServerException(t.message, t.responseCode);
                        }
                    }
                });
    }

    protected List<MultipartBody.Part> arrayImagePart(String key, List<String> path) {

        List<MultipartBody.Part> parts = null;

        if (path != null && !path.isEmpty()) {
            parts = new ArrayList<>();
            for (int i = 0; i < path.size(); i++) {
                File file = new File(path.get(i));
                RequestBody requestBody = MultipartBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part formData = MultipartBody.Part.createFormData(key + "[" + i + "]",
                        file.getName(), requestBody);
                parts.add(formData);
            }
        }

        return parts;
    }

    protected RequestBody formRequestBody(String value) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), value);
    }

    protected MultipartBody.Part singleImagePart(String key, String path) {
        MultipartBody.Part formData = null;
        if (!TextUtils.isEmpty(path)) {
            File file = new File(path);
            RequestBody requestBody = MultipartBody.create(MediaType.parse("image/*"), file);
            formData = MultipartBody.Part.createFormData(key, file.getName(), requestBody);
        }
        return formData;
    }



}
