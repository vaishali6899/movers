package com.movers.data.datasource;

import com.movers.core.AppExecutors;
import com.movers.data.repository.PartyLocatorRepository;
import com.movers.data.service.PartyLocatorService;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by hlink21 on 30/11/17.
 */
@Singleton
public class PartyLocatorDataSource extends BaseDataSource implements PartyLocatorRepository {

    private final PartyLocatorService collegeService;

    @Inject
    public PartyLocatorDataSource(AppExecutors appExecutors, PartyLocatorService collegeService) {
        super(appExecutors);
        this.collegeService = collegeService;
    }

//    @Override
//    public Single<CMSDatam> getCMSPage(HashMap<String, String> hashMap) {
//        return execute(collegeService.getCMSPage(hashMap));
//    }
}
