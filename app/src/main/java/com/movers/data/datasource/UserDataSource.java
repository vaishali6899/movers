package com.movers.data.datasource;

import com.movers.data.repository.UserRepository;
import com.movers.data.service.UserService;
import com.movers.ui.authentication.model.register.RegisterResponse;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

/**
 * Created by hlink21 on 30/11/17.
 */
@Singleton
public class UserDataSource extends BaseDataSource implements UserRepository {

    private final UserService userService;

    @Inject
    public UserDataSource(AppExecutors appExecutors, UserService userService) {
        super(appExecutors);
        this.userService = userService;
    }

    @Override
    public Single<RegisterResponse> register(HashMap<String, String> map) {
        return execute(userService.register(map));
    }

    @Override
    public Single<RegisterResponse> login(HashMap<String, String> map) {
        return execute(userService.login(map));
    }
}
