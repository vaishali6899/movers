package com.movers.data.service;


import com.movers.data.URLFactory;
import com.movers.ui.authentication.model.register.RegisterResponse;

import java.util.HashMap;

import io.reactivex.Single;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by hlink21 on 30/11/17.
 */

public interface UserService {
//
//    @FormUrlEncoded
//    @POST(URLFactory.Method.LOGIN)
//    Single<RegisterResponse> login(@FieldMap HashMap<String, String> map);
    @FormUrlEncoded
    @POST(URLFactory.Method.register)
    Single<RegisterResponse> register(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST(URLFactory.Method.login)
    Single<RegisterResponse> login(@FieldMap HashMap<String, String> map);
}
