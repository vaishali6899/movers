package com.movers.data;

/**
 * Created by hlink21 on 11/5/17.
 */

public class URLFactory {


    public static final String API_PATH = "http://dev.sinontechs.com/movers/index.php/api/";

    // API Methods
    public interface Method {
        public String login = API_PATH + "login";
        public String register = API_PATH + "register";
        public String get_profile = API_PATH + "get_profile";
        public String edit_profile = API_PATH + "edit_profile";

    }

}
