package com.movers.data.Tags;

public class UserTags {

    public static String email = "email";
    public static String name = "name";
    public static String last_name = "last_name";
    public static String password = "password";
    public static String password_confirmation = "password_confirmation";
    public static String postcode = "postcode";
    public static String contact_number = "contact_number";
    public static String type = "type";
    public static String user_type = "user_type";
    public static String company_name = "company_name";
    public static String  cemail_address = "  cemail_address";

    public static String ccontact_number = "ccontact_number";
    public static String caddress = "caddress";
    public static String company_mc = "company_mc";
    public static String company_dot = "company_dot";
    public static String device_type = "device_type";
    public static String device_token = "device_token";
    public static String login_source = "login_source";
}
