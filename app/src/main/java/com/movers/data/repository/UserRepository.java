package com.movers.data.repository;


import com.movers.ui.authentication.model.register.RegisterResponse;

import java.util.HashMap;

import io.reactivex.Single;

/**
 * Created by hlink21 on 30/11/17.
 */

public interface UserRepository {

    Single<RegisterResponse> register(HashMap<String, String> map);
    Single<RegisterResponse> login(HashMap<String, String> map);

}
