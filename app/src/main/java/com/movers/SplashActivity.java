package com.movers;

import android.os.Bundle;

import androidx.annotation.Nullable;

import android.view.WindowManager;

import com.movers.base.BaseActivity;
import com.movers.core.Common;
import com.movers.di.component.ActivityComponent;
import com.movers.ui.authentication.fragment.SplashFragment;
import com.movers.utilsfile.HelperMethod;

import java.io.Serializable;


public class SplashActivity extends BaseActivity {
    @Override
    public int findFragmentPlaceHolder() {
        return R.id.placeHolder;
    }

    @Override
    public int findContentView() {
        return R.layout.activity_splash;
    }

    @Override
    public void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        HelperMethod.keyhash(this);


        if (((Serializable) getIntent().getSerializableExtra(Common.ACTIVITY_FIRST_PAGE)) != null) {
            Class aClass = (Class) getIntent().getSerializableExtra(Common.ACTIVITY_FIRST_PAGE);
            load(aClass, false).replace(false);
        } else {

            load(SplashFragment.class, false).replace(false);


        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

    }


    @Override
    public void toggleDrawer() {

    }

}